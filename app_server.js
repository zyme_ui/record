var dgram  = require('dgram');
var path = require('path');
var fs = require('fs');
var os = require('os');
var _ = require('underscore');
var nconf = require('nconf');
var async = require('async');
//var drecorder = require('./build/Debug/drecorder');
var drecorder = require('./drecorder');
var system_stats = require('./system_stats');
var version = require('./package').version

var mtg_proto = require('./mtg_proto');
var net_utils = require('./net_utils');

const RECORDER_TRACE_ALL = 65535;

var g_server_socket = dgram.createSocket('udp4');
var g_sessions = {};
var g_call_sessions = {};
var g_requests = {};
var g_processed_total = 0;
var g_processed_succeed = 0;
var g_processed_failed = 0;

GLOBAL.g_app_status = {};

nconf.argv()
     .env()
     .file({ file: 'etc/config.json' })
     .defaults({'recording_path': '/tmp', 'tmp_path':'/tmp', 'recorder_log_path':'/tmp/drecord.log', 'recorder_log_level':RECORDER_TRACE_ALL
        , 'first_idle':15000, 'stream_idle':1000});

var logger = require('./logger_factory').create(nconf.get("logger_factory"));

var g_ip_uint32 = net_utils.addr2long(nconf.get("server_ip"));
var g_status_file = path.join(nconf.get("status_path"), nconf.get("server_id")+".status");


function make_request_idx(request_id, call_id, peer_info) {
  return peer_info.address.replace(/\./g, '_')+"_"+peer_info.port+"_"+request_id+'_'+call_id;
}

function has_request(idx) {
  return idx in g_requests;
}

function lock_request(idx) {
  g_requests[idx] = true;
}

function incr_processed(is_failed) {
  g_processed_total++;
  if(is_failed) {
    g_processed_failed++;
  } else {
    g_processed_succeed++;
  }
}

function unlock_request(idx) {
  delete g_requests[idx];
}

function generate_session_id(call_id) {
  return ""+(new Date()).getTime()+"_"+call_id;
}

function build_session(session_id, call_id, caller, called, channels, sockets, peer_info, out_path, tmp_path) {
  logger.info("build session "+session_id+" callid "+call_id+" peer_info "+JSON.stringify(peer_info));
  if(g_sessions[session_id]) {
    throw "session exists";
  }
  var now = (new Date).getTime();
  var chan_recv_timestamps = [];
  var chan_recv_packets_per_sec = [];
  for(var i = 0; i < channels.length; ++i) {
    chan_recv_timestamps.push(now);
    chan_recv_packets_per_sec.push([]); //rpps is two dimension array like [[{ts_sec:100, value:10}], [{ts_sec:101, value:11}]].
  }
  var session = {
    id:session_id
      , call_id:call_id
      , caller:caller
      , called:called
      , channels:channels
      , chan_sockets:sockets
      , peer_info:peer_info
      , chan_recv_timestamps:chan_recv_timestamps
      , first_packet:true
      , create_time:new Date
      , chan_recv_packets:[].slice.apply(new Uint8Array(channels.length))
      , chan_recv_packets_per_sec: chan_recv_packets_per_sec
      , chan_stats:[].slice.apply(new Uint8Array(channels.length))
      , tmp_path:tmp_path
      , out_path:out_path
  };
  //wait for 5s before receive first packet
  session.idle_timer = setTimeout(function() {
    if(!is_working(session)) {
      session.idle_timer = null;
      stop_record_when_error("[DRecord "+session_id+"] wait first stream pakcet too long!", session.id, session.call_id, 0);
    }
  }, nconf.get("first_idle"));
  g_sessions[session_id] = session;
  g_call_sessions[make_call_info(call_id, peer_info)] = session;
}

function schedule_stream_idle_timer(session) {
  if(session.idle_timer) {
    clearTimeout(session.idleTimer);
    session.idle_timer = null;
  }
  session.idle_timer = setTimeout(function() {
    session.idle_timer = null;
    if(!is_working(session)) {
      stop_record_when_error("[DRecord "+session.id+"] wait stream pakcet too long!", session.id, session.call_id, 0);
    } else {
      schedule_stream_idle_timer(session);
    }
  }, nconf.get("stream_idle"));
}

function get_session(session_id) {
  return g_sessions[session_id];
}

function make_call_info(call_id, peer_info) {
  return peer_info.address+"_"+peer_info.port+"_"+call_id;
}

function remove_session(session) {
  var call_info = make_call_info(session.call_id, session.peer_info);
  delete g_sessions[session.id];
  delete g_call_sessions[call_info];
}

function get_session_by_peer(call_id, peer_info) {
  var call_info = make_call_info(call_id, peer_info);
  return g_call_sessions[call_info];
}

function session_on_received(session, chan_id) {
  //#WARNING: the implement assume chan_id can direct use as index
  var ts = (new Date).getTime();
  var ts_sec = Math.round(ts/1000);

  if(!session) {
    return;
  }
  
  session.chan_recv_timestamps[chan_id] = ts;
  session.chan_recv_packets[chan_id] += 1;
  var chan_rpps = session.chan_recv_packets_per_sec[chan_id];
  if(chan_rpps.length && chan_rpps[chan_rpps.length-1].ts_sec == ts_sec) {
    chan_rpps[chan_rpps.length-1].value += 1;
  } else {
    chan_rpps.push({ts_sec:ts_sec, value:1});
    if(chan_rpps.length > 60) { // keep one minute samples
      chan_rpps.shift();
    }
  }
}

/*function mv(src, dst, callback) {*/
  //var rs = fs.createReadStream(src);
  //var ws = fs.createWriteStream(dst);
  //ws.on('finish', function() {
    //fs.unlink(src, callback);
  //}).on('error', function(error) {
    //callback(error);
  //});
  //rs.pipe(ws);
/*}*/

function session_stopped(session) {
  var try_times = 0;
  if(!session.first_packet) {
    logger.info("session chan 0 packets:"+session.chan_recv_packets[0]+",chan 1 packets:"+session.chan_recv_packets[1]
        +",during time:"+((new Date)-session.create_time));
    var fun = function() {
      fs.rename(session.tmp_path, session.out_path, function(error) {
        if(error) {
          logger.debug("rename file error:"+error);
          if(try_times < 3) {
            logger.debug('try rename again '+session.tmp_path);
            try_times += 1;
            setTimeout(fun, try_times*2000);
          }
        } else {
          logger.debug('record file rename to:'+session.out_path);
        }
      });
    };
    fun();
  } else {
    logger.debug('session stopped at 0 packet');
  }
}

function make_out_path(session_id, caller, called, fmt) {
  var date = new Date;
  var darray = date.toISOString().split('T');
  //var tmpdir = path.join(nconf.get("tmp_path"), darray[0]);
  var tmpdir = path.join(nconf.get("recording_path"), darray[0]);
  var outdir = path.join(nconf.get("recording_path"), darray[0]);
  try {
    fs.mkdirSync(tmpdir);
    fs.mkdirSync(outdir);
  } catch(e) {
  }
  return {out_path:path.join(outdir, session_id+"_"+caller+"_"+called+"."+fmt)
    , tmp_path:path.join(tmpdir, session_id+"_"+caller+"_"+called+"."+fmt+'.tmp')};
}

function stop_record_when_error(error, session_id, call_id, chan_id) {
  var session = get_session(session_id);
  if(!session) {
    return;
  }
  logger.info("%s stop_record_when_error %s", session_id, error);
  if(error.stack) {
    //console.log("stack:"+error.stack);
    logger.info("stack:"+error.stack);
  }
  remove_session(session);
  if(session.recording_timer) {
    //nothing todo...
    //session.recording_timer.cancelTimeout();
  }
  if(session.idle_timer) {
    clearInterval(session.idle_timer);
  }
  if(session.sockets) {
    session.sockets.forEach(function(socket) {
      socket.close();
    });
  }
  drecorder.stop_recorder(session_id);
  session_stopped(session);
  incr_processed(true);
}

function is_working(session) {
  var deadline = (new Date).getTime() - 5000;
  //logger.log("[is_working] deadline:"+deadline+"    chan_recv_timestamps:"+session.chan_recv_timestamps);
  for(var i = 0; i < session.chan_recv_timestamps.length; ++i) {
    if( session.chan_recv_timestamps[i] > deadline ) {
      return true;
    }
  }
  //console.log("[is_working] not working now!!");
  logger.info("%s not working exceed deadline:%s, %d", session.id, session.chan_recv_timestamps, deadline);
  return false;
}

function get_millisecs(nano) {
  return nano[0]*1000+nano[1]/1000000;
}

function try_start_recording_timer(session_id, chan_id, method) {
  var session = get_session(session_id);
  if(session) {
    if(session.recording_timer) { //timer alread started
      return;
    }

    //start timer when received first media packet
    //var timer = new NanoTimer();
    var ts = process.hrtime();
    var interval = 10;
    var interval_str = '10m';
    var stopped = false;
    var timer_fun;

    //session.recording_timer = timer;
    var reschedule = function(waitTime) {
      if(stopped) {
        logger.info("record stopped");
        return;
      }
      //waitTime is actual wait time from setTimeout to execute timer fun.
      //we assume waitTime >= interval always is true.
      var delta = Math.round(waitTime/1000000);
      var val; 
      if(delta > interval) {
        //val = ''+(interval-(delta & (interval - 1)))+'m'; // get modulo
        //val = ''+((interval<<1)-delta)+'m';
        val = ((interval<<1)-delta);
      } else {
        //val = interval_str;
        val = interval;
      }

      //timer.setTimeout(timer_fun, [], val, reschedule);
      session.recording_timer = setTimeout(timer_fun, val);
    };
    if(method == "sync") {
      timer_fun = function() {
        var now = process.hrtime();
        try {
          drecorder.record_sync(session_id);
          reschedule(now);
        } catch(err) {
          logger.log("stop record:"+err.stack);
          stopped = true;
          stop_record_when_error(err, session_id, session.call_id, chan_id);
        }
      };
    } else {
     timer_fun = function() {
        drecorder.record(session_id, function(err) {
          var now = process.hrtime();
          if(err) {
            stopped = true;
            stop_record_when_error(err, session_id, session.call_id, chan_id);
          } else {
            reschedule(now);
          }
        });
      };
    }
    //timer.setTimeout(timer_fun, [], interval_str, reschedule);
    session.recording_timer = setTimeout(timer_fun, interval);
    //timer.setInterval(timer_fun, [], interval_str, reschedule);
  } else {
    logger.info("SHOULD NOT REACH HERE! "+session_id);
  }
}

function start_channel_socket(session_id, call_id, chan_id, peer_info, bind_callback, method) {
  var socket = dgram.createSocket('udp4');
  var addr_info;
  var session;
  var packet_count = 0;
  socket.on("error", function (err) {
    logger.info("chan socket error:\n" + err.stack);
    if(err && err.syscall == "bind") {
      bind_callback(err); //socket bind error
      return;
    } else {
      if(socket) {
        socket.close();
      }
      socket = null;
      stop_record_when_error(err, session_id, call_id, chan_id);
      return;
    }
  });

  socket.on("message", function (msg, rinfo) {
    var t1 = process.hrtime();
    //logger.info("%s received packet from %s at %d", addr_info, rinfo.address+":"+rinfo.port, get_millisecs(t1));
    /*console.log("got media("+msg.length+"B) at " +*/
      /*rinfo.address + ":" + rinfo.port);*/
    if(rinfo.address != peer_info.address) {
      //console.log("discard packet because unknown source");
      return;
    }
    if(msg.length != mtg_proto.SIZE_MEDIA_PACKET) {
      //console.log("discard packet because size incorrect:"+msg.length);
      return;
    } 

    if(!session) {
      session = get_session(session_id);
    }

    if(!session) {
      logger.info("can't found session:%s!", session_id);
      return;
    }

    session_on_received(session, chan_id)

    try {
      if(method == "sync") {
        try {
          var t2 = process.hrtime(t1);
          drecorder.channel_on_data_sync(session_id, chan_id, msg);
          //logger.info("channel_on_data consume %dms", get_millisecs(t2));
        } catch(error) {
          logger.info(session_id+" channel_on_data_sync "+chan_id+" error "+error);
        }
      } else {
        drecorder.channel_on_data(session_id, chan_id, msg, function(error) {
          var t2 = process.hrtime(t1);
          //logger.info("channel_on_data consume %dms", get_millisecs(t2));
          if(error) {
            logger.info(session_id+" channel_on_data "+chan_id+" error "+error);
          } 
        });
      }

      if(!packet_count) {
        var session = get_session(session_id);
        if(session && session.first_packet) {
          session.first_packet = false;
          try_start_recording_timer(session_id, chan_id, method);
          schedule_stream_idle_timer(session);
        }
      }
      packet_count++;
    } catch(ex) {
      logger.info(""+ex+","+ex.stack);
    }
  });

  socket.on("listening", function () {
    var address = socket.address();
    addr_info = address.address + ":" + address.port;
    logger.info("chan socket listening " +
        address.address + ":" + address.port);
    bind_callback(null, socket);
  });

  socket.bind(0); //bind dynamic port
}

///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复<]

    //S8              szCalling[RCD_MAX_NUM_LEN+1];
    //S8              szCalled[RCD_MAX_NUM_LEN+1];

//}RECORD_MSG_S;
///////////////////////////////////////////////////////////////////////////////
function maybe_record_start(index, msg, rinfo) {
  var req = mtg_proto.parse_record_start(msg);
  var error = false;
  if(Object.keys(g_sessions).length >= nconf.get("max_sessions")) {
    logger.info("exceed max record session");
    error = true;
  } else if( get_session_by_peer(req.call_id, rinfo) ) {
    logger.info("record session already exists");
    error = true;
  }
  if(error) {
    ack = mtg_proto.make_record_start_ack(req.request_id, req.call_id, mtg_proto.ERR_RECORD_SERV_UNAVA);
    g_server_socket.send(ack, 0, ack.length, rinfo.port, rinfo.address, function(error) {
      if(error) {
        logger.info("send ack to " + rinfo.address + ":" + rinfo.port + " error "+error);
      }
    });
    return;
  }

  var session_id = generate_session_id(index);
  var tmp_path, out_path;

  logger.info("received record_start " + JSON.stringify(req) + ", session id is " + session_id);

  try {
    var result = make_out_path(session_id, req.caller, req.called, "wav");
    out_path = result.out_path;
    tmp_path = result.tmp_path;
    drecorder.start_recorder(session_id, req.caller, req.called, {'out_dir':tmp_path, 'format':"wav"});
    //create 2 channels for caller->called and called->caller
    logger.info("add channel");
    var channels = drecorder.add_channels(session_id, 2, {});
    logger.info("start sockets for channels:"+channels);
  } catch(ex) {
    logger.info("exception:"+ex.stack);
  }
  
  var caller_port = 0, called_port = 0;
  async.map(channels, 
      function(chan_id, callback) {
        logger.info("start channel socket "+chan_id);
        start_channel_socket(session_id, req.call_id, chan_id, rinfo, function(error, socket) {
          if(error) {
            callback(error);
          } else {
            callback(null, socket);
          }
        });
      }, 
      function(error, sockets) {
        var ack;
        if(error) {
          logger.info("start channel socket error "+error);
          drecorder.stop_recorder(session_id);
          if(sockets) { //close opened channel socket 
            sockets.forEach(function(s) {
              if(s) {
                s.close();
              }
            });
          }
          ack = mtg_proto.make_record_start_ack(req.request_id, req.call_id, mtg_proto.ERR_RECORD_SERV_UNAVA);
        } else {
          build_session(session_id, req.call_id, req.caller, req.called, channels, sockets, rinfo, out_path, tmp_path);
          ack = mtg_proto.make_record_start_ack(req.request_id, req.call_id, mtg_proto.ERR_SUCCESS
              ,sockets[0].address().port, sockets[1].address().port, g_ip_uint32);
        }
        //first socket for caller->called, second socket for called->caller
        g_server_socket.send(ack, 0, ack.length, rinfo.port, rinfo.address, function(error) {
          if(error) {
            logger.info("send ack to " + rinfo.address + ":" + rinfo.port + " error "+error);
          }
        });
    });
}


///////////////////////////////////////////////////////////////////////////////
//
//typedef struct
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复,  此处不关心, 置0<]
//}LNK_DETECT_MSG_S;
///////////////////////////////////////////////////////////////////////////////
function maybe_link_detect(msg, rinfo) {
  var req = mtg_proto.parse_link_detect(msg);
  logger.info("got link detect req %s", JSON.stringify(req));
  ack = mtg_proto.make_link_detect_ack(req.request_id, req.call_id);
  g_server_socket.send(ack, 0, ack.length, rinfo.port, rinfo.address, function(error) {
    if(error) {
      logger.info("send ack to " + rinfo.address + ":" + rinfo.port + " error "+error);
    }
  });
}

function maybe_record_stop(msg, rinfo) {
  var req = mtg_proto.parse_record_stop(msg);
  var session = get_session_by_peer(req.call_id, rinfo);
  if(!session) {
    logger.info("unknown record session for stop:"+req.call_id+", "+rinfo.address+":"+rinfo.port);
    return;
  }
  drecorder.stop_recorder(session.id);
  session_stopped(session);
  incr_processed(false);
  ack = mtg_proto.make_record_stop_ack(req.request_id, req.call_id);
  g_server_socket.send(ack, 0, ack.length, rinfo.port, rinfo.address, function(error) {
    if(error) {
      logger.info("send ack to " + rinfo.address + ":" + rinfo.port + " error "+error);
    }
  });
}


g_server_socket.on("error", function (err) {
  logger.info("server error:\n" + err.stack);
  g_server_socket.close();
});

function handle_signal_message(msg, rinfo) {
  var req_id = mtg_proto.request_id(msg);
  var call_id = mtg_proto.call_id(msg);
  var r_idx = make_request_idx(req_id, call_id, rinfo);
  var call_info = make_call_info(call_id, rinfo);
  console.log("g_count "+g_count+" msg.length "+msg.length+" req_id "+req_id+" call_id "+call_id);
  g_count ++;
  if(has_request(r_idx)) {
    logger.info("server got repeat request %s", r_idx);
    return;
  } else {
    logger.debug("server got request %s", r_idx);
  }
  lock_request(r_idx);
  try {
    if(msg.length == mtg_proto.SIZE_CMD_RECORD_START) {
      try {
        maybe_record_start(r_idx, msg, rinfo);
      } catch(e) {
        if( e instanceof TypeError) {
          maybe_record_stop(msg, rinfo);
        } else {
         logger.info("maybe_record_start failed:"+e+", stack:"+e.stack);
        }
      }
    } else if(msg.length == mtg_proto.SIZE_CMD_LNK_DETECT) {
      maybe_link_detect(msg, rinfo);
    /*} else if(msg.length == mtg_proto.SIZE_CMD_RECORD_STOP) {*/
      /*maybe_record_stop(msg, rinfo);*/
    }
    else {
      logger.info("invalid message");
      return;
    }
  } catch(ex) {
    logger.info("handle message failed:"+ex.stack);
  }
  unlock_request(r_idx);
}

var g_count = 0;
g_server_socket.on("message", function (msg, rinfo) {
  logger.info("server got: " + " from " +
    rinfo.address + ":" + rinfo.port+" size:"+msg.length);
  handle_signal_message(msg, rinfo);
});

drecorder.init(nconf.get("recorder_log_path"), nconf.get("recorder_log_level"));

function write_status(runtime_stats) {
  var all_stats = drecorder.internal_status();
  if(!_.isEmpty(all_stats)) {
    if(all_stats.runtime) {
      var count = Object.keys(g_sessions).length;
      var status_obj = {engine_stats:all_stats.runtime
            , app_status:{sessions_count:count, processed_total:g_processed_total, processed_failed:g_processed_failed}
            , product_info: {product_id:nconf.get("product_id"), product_name:nconf.get("product_name")
              , version:version, server_id:nconf.get("server_id")}
            , runtime:runtime_stats
            , app_config:{listen_ip:nconf.get("server_ip"), listen_port:nconf.get("server_port"), max_sessions:nconf.get("max_sessions")}
            , timestamp: (new Date).getTime()
          };
      GLOBAL.g_app_status = status_obj;
      logger.info("sessions count: %d, recorder status: %s", count, JSON.stringify(all_stats.runtime));
      logger.info("cpu loadavg %s\%, disk usage %s\%, mem usage %s\%, network rx rate %dkbps, tx rate %dkbps"
          , runtime_stats.loadavg_1m
          , (runtime_stats.disk_used/(runtime_stats.disk_used+runtime_stats.disk_available))*100
          , (runtime_stats.free_memory/runtime_stats.total_memory)*100
          , runtime_stats.rx_rate
          , runtime_stats.tx_rate);
      fs.writeFile(g_status_file
          , JSON.stringify(status_obj)
        );
    }
    for(var session_id in all_stats.sessions) {
      var session = g_sessions[session_id];
      if(session) {
        session.chan_stats = all_stats[session_id];
      }
    }
  }
};

///////////////////////////////////////////////////////////////////////////////
// socket listener
///////////////////////////////////////////////////////////////////////////////
var g_init_callback = null
function on_bind() {
  var address = g_server_socket.address();
  logger.info("server listening " +
      address.address + ":" + address.port);
  logger.info("media server ip "+g_ip_uint32);
  g_init_callback(null, {
    product_id: nconf.get("product_id")
    , product_name: nconf.get("product_name")
    , server_id: nconf.get("server_id")
    , status_path: path.join(nconf.get('status_path'), ''+nconf.get("server_id")+'.status')
    , recording_path: nconf.get('recording_path')
    , backup_path: nconf.get('backup_path')
  });
}

function on_error(error) {
  if(g_init_callback) {
    g_init_callback(error);
  }
  g_server_socket.removeListener("error", on_error);
}

g_server_socket.on("listening", on_bind);
g_server_socket.on("error", on_error);

function init(callback) {
  if(g_init_callback) {
    process.nextTick(function() {
      callback("already_initialized");
    });
    return;
  }

  system_stats.start(5000, {ip:nconf.get("server_ip"), work_dir:nconf.get("recording_path")})
  .on("error", function(error) {
    logger.info("collect system stats error:"+JSON.stringify(error));
  }).on("collected", function(info) {
    write_status(info);
  });

  g_init_callback = callback;
  //start server and run loop
  if(nconf.get("server_ip")) {
    g_server_socket.bind(nconf.get("server_port"), nconf.get("server_ip"));
  } else {
    g_server_socket.bind(nconf.get("server_port"));
  }
}

function try_direct_start() {
  if(process.argv[1] == path.join(process.cwd(), 'app_server.js')) {
    init(function(error) {
      if(error) {
        console.log("start server error:"+error);
        process.exit(1);
      }
    });
  }
}

try_direct_start();

exports.init = init;
