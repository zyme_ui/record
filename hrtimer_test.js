

var interval = 1000*1000; //1ms
var last = process.hrtime();


function hr_task() {
  var now = process.hrtime();
  if(now[0]>last[0] || (now[1]-last[1])>interval) {
    //console.log("timeout "+(new Date).getTime() + " now "+now + " last "+last);
    last = now;
  }
  setImmediate(task);
}


function get_millisecs(nano) {
  return Math.round(nano[0]*1000+nano[1]/1000000);
}

function fast_task() {
  setInterval(function() {
    var now = process.hrtime();
    console.log("timeout "+get_millisecs(now));
  }, 10);
}

function avg_task() {
  var max = 1000000;
  var avg1 = 0;
  var avg2 = 0;
  var avg3 = 0;
  var i = 0;
  for(i = 0; i < max; ++i) {
    var v = Math.random()*100;
    avg1 += v;
    if(avg2 == 0) {
      avg2 = v;
    } else {
      avg2 = (avg2*2 + v)/2;
    }
    if(avg3 == 0) {
      avg3 = v;
    } else {
      avg3 = (avg3+v)/2;
    }
  }
  console.log("avg1:"+(avg1 / max));
  console.log("avg2:"+avg2);
  console.log("avg3:"+avg3);
}

//fast_task();
avg_task();

console.log("argv:"+process.argv);
