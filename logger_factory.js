default_config = 
  {type:"winston",
    settings:{transports:
      ["Console", {"level":'debug', "colorize":true, "timestamp":true}]}
  };

function explore(obj, key_seq) {
  var j = obj;
  key_seq.forEach(function(key) {
    j = j[key];
  });
  return j;
}


function create_winston(settings) {
  var winston = require('winston');
  var transports = [];
  for(var i = 0; i < settings.transports.length; ++i) {
    var type = settings.transports[i][0]; //0 index is transport type name
    var namespace = type.split('.');
    if(namespace) {
      if(namespace[0] == 'winston') {
        namespace = namespace.slice(1);
      } else {
        throw "now logger namespace must be winston!!";
      }
      var constructor = explore(winston, namespace);
      transports.push(new constructor(settings.transports[i][1])); //1 index is then transport settings
    }
  }
  settings.transports = transports;
  return new (winston.Logger)(settings);
}

function create(config) {
  if(!config) {
    config = default_config;
  }
  if(config.type == "winston") {
    return create_winston(config.settings);
  }
}

module.exports.create = create;
