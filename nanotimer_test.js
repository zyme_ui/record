var async = require('async');
var NanoTimer = require('nanotimer');

function get_millisecs(nano) {
  return nano[0]*1000+nano[1]/1000000;
}

function try_start_recording_timer(total, callback) {
    //start timer when received first media packet
    var timer = new NanoTimer();
    var ts = process.hrtime();
    var interval = 10;
    var interval_str = '10m';
    var stopped = false;
    var timer_fun;
    var seq = 0;

    var reschedule = function(info) {
      if((get_millisecs(process.hrtime(ts))>total)) {
        callback();
        return;
      }
      seq ++;
      var delta = Math.round(info.waitTime/1000000);
      var val; 
      if(delta > interval) {
        val = ''+(interval-(delta-interval))+'m';
      } else {
        val = interval_str;
      }

      //console.log("seq:"+seq+" "+val+" "+delta+" "+interval);

      timer.setTimeout(timer_fun, [], val, reschedule);
    };
    var _timer_fun = function() {
      //console.log("record timeout:"+get_millisecs(process.hrtime()));
    };
    timer_fun = _timer_fun;
    timer.setTimeout(timer_fun, [], interval_str, reschedule);
}

/*try_start_recording_timer(100000, function() {*/
  //console.log("test_over");
/*});*/

var index_array = [];
for(var i = 0; i < 1; ++i) {
  index_array.push(i);
}

async.map(index_array, 
    function(i, callback) {
      try_start_recording_timer(100000, function() {
        console.log("test over:"+i);
        callback();
      });
    },
    function() {
      console.log('all tests run over!');
    }
);
