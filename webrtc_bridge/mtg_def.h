#ifndef _MTG_DEF_H
#  define _MTG_DEF_H

#pragma pack(push)
#pragma pack(1)

#ifdef __cplusplus
extern "C" {		// shouldn't this be in the DRIVER_INIT macro?
#endif


#define _MTG_RTP_HEADER_FIELDS \
    uint32_t magic; \
    uint16_t seq; \
    uint32_t timestamp

typedef struct _mtg_rtp_header {
    _MTG_RTP_HEADER_FIELDS;
} mtg_rtp_header;

/*MTG use G711 alaw*/
#define MTG_PAYLOAD_LEN    160
#define MTG_PTIME    20
#define MTG_PAYLOAD_TYPE   8
#define MTG_PAYLOAD_MAGIC_1  0x01550000
#define MTG_PAYLOAD_MAGIC_2  0x02550000

typedef struct _mtg_rtp_g711 {
    _MTG_RTP_HEADER_FIELDS;
    uint8_t payload[MTG_PAYLOAD_LEN];
} mtg_rtp_g711;


#ifdef __cplusplus
}
#endif

#pragma pack(pop)

#endif /* _MTG_DEF_H */
