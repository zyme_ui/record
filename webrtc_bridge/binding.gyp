{
  'variables': {
    'target_arch%': 'ia32',           # set v8's target architecture
    'host_arch%': 'ia32',             # set v8's host architecture
  },
  'make_global_settings': [
    ['CXX','$(WEBRTC_ROOT)/third_party/llvm-build/Release+Asserts/bin/clang++'],
    ['LINK','$(WEBRTC_ROOT)/third_party/llvm-build/Release+Asserts/bin/clang++'],
  ],
  "targets": [
    {
      "target_name": "drecorder",
      "sources": ["drecorder_bridge.cpp"],
      "include_dirs": [
        "$(WEBRTC_ROOT)",
		"<!(node -e \"require('nan')\")"
	  ],
      "defines": [
	"MYLIBS=-lsystem_wrappers -lcommon_audio_sse2 -lG711 -laudio_coding_module -lmedia_file -laudio_processing_sse2 -lneteq -lPCM16B -lgflags -lcommon_audio -lrtc_base_approved -lopenmax_dl -lCNG -lrtp_rtcp -lpaced_sender -lremote_bitrate_estimator -lrbe_components -lfield_trial -lmetrics_default"
      ],
      "ldflags" : ["-Wall", "-Wl,-z,now -Wl,-z,relro -Wl,--fatal-warnings -Wl,-z,noexecstack -fuse-ld=gold -B$(WEBRT_ROOT)/third_party/binutils/Linux_x64/Release/bin -Wl,--disable-new-dtags -Wl,--icf=safe"
	, "-lrt -lm"
      ],
#      "ldflags" : ["-Wall", "-Wl,-z,now -Wl,-z,relro -Wl,--fatal-warnings -Wl,-z,noexecstack -fuse-ld=gold -B$(WEBRT_ROOT)/third_party/binutils/Linux_x64/Release/bin -Wl,--disable-new-dtags -Wl,--icf=safe"
#        , "-Wl,--start-group"
#        , "-lsystem_wrappers", "-lcommon_audio_sse2", "-lG711", "-laudio_coding_module", "-lmedia_file"
#        ,"-laudio_processing_sse2", "-lneteq","-lPCM16B", "-lgflags", "-lcommon_audio", "-lrtc_base_approved"
#        , "-lopenmax_dl", "-lCNG", "-lrtp_rtcp", "-lpaced_sender", "-lremote_bitrate_estimator"
#        , "-lrbe_components", "-lfield_trial", "-lmetrics_default"
#        , "-Wl,--end-group"
#	, "-lrt -lm"
#      ],
      #"libraries" : [ "system_wrappers", "common_audio_sse2", "G711", "audio_coding_module", "media_file"
      #  ,"audio_processing_sse2", "neteq","PCM16B", "gflags", "common_audio", "rtc_base_approved"
      #  , "openmax_dl", "CNG", "rtp_rtcp", "paced_sender", "remote_bitrate_estimator"
      #  , "rbe_components", "field_trial", "metrics_default"
      #],

	  "library_dirs": ["$(WEBRTC_LIB)/Release",],
      'conditions': [
          [ 'OS in "linux freebsd openbsd solaris"', {
              'cflags_cc!': ['-fno-rtti', '-fno-exceptions'],
              'cflags_cc': ['-frtti', '-fexceptions', '-std=c++11', '-fPIC'],
              'ldflags': ['-fPIC', '-v'],
          }],
          ['OS=="mac"', {
              'xcode_settings': {
                  'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',
                  'GCC_ENABLE_CPP_RTTI': 'YES',
                  'OTHER_CPLUSPLUSFLAGS' : ['-std=c++11','-stdlib=libc++'],
                  'OTHER_LDFLAGS': [],
                  'MACOSX_DEPLOYMENT_TARGET': '10.7'
              },
              'conditions': [
                  ['target_arch=="ia32"', {
                    'xcode_settings': {'ARCHS': ['i386']},
                  }],
                  ['target_arch=="x64"', {
                    'xcode_settings': {'ARCHS': ['x86_64']},
                  }],
                ]
          }]
      ],
    }
  ]
}
