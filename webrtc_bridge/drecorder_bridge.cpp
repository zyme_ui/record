#include <netinet/in.h>
/*#define IS_BIG_ENDIAN (!*(unsigned char *)&(uint16_t){1})*/

//#if (IS_BIG_ENDIAN)

//#define le16toh(val) ((((uint16_t)val&0xff00) >> 8)|(((uint16_t)val&0x00ff) << 8))
//#define le32toh(val) ((((uint32_t)val&0xff000000) >> 24)|(((uint32_t)val&0x00ff0000) >> 8)|(((uint32_t)val&0x0000ff00) << 8)|(((uint32_t)val&0x000000ff) << 24))

//#else

//#define le16toh(val) val
//#define le32toh(val) val


/*#endif*/


/*#define read_le16(buf) ((((uint8_t*)buf)[0] >> 8)|(((uint8_t*)buf)[1] << 8))*/
/*#define read_le32(buf) ((((uint8_t*)buf)[0] >> 16)|(((uint8_t*)buf)[1] >> 16)|(((uint8_t*)buf)[2] << 16)|(((uint8_t*)buf)[3] << 16))*/

#define _HOST_IS_LITTLE_ENDIAN true

#if (_HOST_IS_LITTLE_ENDIAN)
inline uint16_t read_le16(uint8_t* val) {
    return *((uint16_t*)val);
}

inline uint32_t read_le32(uint8_t* val) {
    return *((uint32_t*)val);
}
#else
inline uint16_t read_le16(uint8_t* val) {
    uint16_t v0;
    ((uint8_t*)&v0)[0] = val[1];
    ((uint8_t*)&v0)[1] = val[0];
    return v0;
}

inline uint32_t read_le32(uint8_t* val) {
    uint32_t v0 = val[0];
    uint32_t v1 = (uint32_t)val[1] << 8;
    uint32_t v2 = (uint32_t)val[2] << 16;
    uint32_t v3 = (uint32_t)val[3] << 24;
    return v0 | v1 | v2 | v3;
}
#endif

template<int PAYLOAD_LEN, int PACKET_TIME>
uint32_t webrtc_packet_timestamp(uint32_t base, uint32_t now) {
    return base + ((now - base)/PACKET_TIME)*PAYLOAD_LEN;
}

#include <string>
/*#include <tr1/memory>*/
/*#include <tr1/unordered_map>*/
#include <memory>
#include <unordered_map>
#include <list>
#include <vector>
#include <sstream>
#include <exception>
#include <stdexcept>
#include <node.h>
#include <node_buffer.h>
#include <uv.h>
#include "nan.h"

#include "mtg_def.h"

#include <webrtc/common_types.h>
#include <webrtc/system_wrappers/interface/trace.h>
#include <webrtc/system_wrappers/interface/clock.h>
#include <webrtc/common_audio/signal_processing/include/signal_processing_library.h>
#include <webrtc/modules/interface/module_common_types.h>
#include <webrtc/modules/audio_coding/neteq/interface/neteq.h>
#include <webrtc/modules/media_file/interface/media_file.h>
#include <webrtc/modules/media_file/interface/media_file_defines.h>

#define DR_LOG(module, level, msg, ...) \
    webrtc::Trace::Add(level, module, 0, msg, ## __VA_ARGS__)

#define DR_DEBUG(msg, ...) DR_LOG(webrtc::kTraceUndefined, webrtc::kTraceDebug, msg, ## __VA_ARGS__)
#define DR_INFO(msg, ...) DR_LOG(webrtc::kTraceUndefined, webrtc::kTraceInfo, msg, ## __VA_ARGS__)
#define DR_ERROR(msg, ...) DR_LOG(webrtc::kTraceUndefined, webrtc::kTraceError, msg, ## __VA_ARGS__)

// ...

using namespace v8;
/*using std::tr1::shared_ptr;*/
//using std::tr1::unordered_map;

using std::shared_ptr;
using std::unordered_map;

class dr_exception : public std::exception {
public:
  dr_exception() {}
  dr_exception(const char* msg) : str_(msg) {
  }
  dr_exception(const std::string& str) : str_(str) {
  }
  dr_exception(const std::ostringstream& oss) : str_(oss.str()) {
  }
  dr_exception(const dr_exception& other) : str_(other.str_) {
  }
  
  dr_exception& operator= (const dr_exception& other) {
    if(&other != this) {
      str_ = other.str_;
    }
    return *this;
  }

  virtual ~dr_exception() override {
  }

  virtual const char* what() const noexcept override {
    return str_.empty() ? "dr_exception" : str_.c_str();
  }

private:
  std::string str_;
};

/*template<class TypeTrait>*/
//struct _v8_trait {
  //template<class CharT>
  //Handle<String> decorate(const CharT* msg) {
    //return String::New(msg);
  //}

  //template<class CharT, class Trait, class Alloc>
  //Handle<String> decorate(const std::basic_ostringstream<CharT, Trait, Alloc>& oss) {
    //return decorate(oss.str().c_str());
  //}
//};

//template<class TypeTrait>
//struct _std_trait {
  //template<class CharT>
  //const CharT* decorate(const CharT* msg) {
    //return msg;
  //}

  //template<class CharT, class Trait, class Alloc>
  //const CharT* decorate(const std::basic_ostringstream<CharT, Trait, Alloc>& oss) {
    //return oss.str().c_str();
  //}
//};



//class _ExceptionUtil {
  //template<class CharT, class Trait, class Alloc, template<class Exception> class _DTrait>
  //static void dr_throw_exception(const std::basic_ostringstream<CharT, Trait, Alloc>& oss, _DTrait<Exception>) {
    //throw Exception(_DTrait<Exception>::decorate(oss));
  //}

  //template<class CharT, class Trait, class Alloc, template<class Exception> class _DTrait>
  //static void dr_throw_exception(std::basic_ostringstream<CharT, Trait, Alloc>& oss, const char* file, int lineno, _DTrait<Exception>) {
    //oss << ", "<< file << ":" << lineno;
    //_DTrait<Exception>::decorate(oss);
    //throw Exception(msg);
  //}
//};

//class _DebugExceptionUtil {
  //template<class CharT, class Trait, class Alloc, template<class Exception> class _DTrait>
  //static void dr_throw_exception(const std::basic_ostringstream<CharT, Trait, Alloc>& oss, _DTrait<Exception>) {
    //const char* msg = _DTrait<Exception>::decorate(oss);
    //DR_ERROR(msg);
    //throw Exception(msg);
  //}

  //template<class CharT, class Trait, class Alloc, template<class Exception> class _DTrait>
  //static void dr_throw_exception(std::basic_ostringstream<CharT, Trait, Alloc>& oss, const char* file, int lineno, _DTrait<Exception>) {
    //oss << ", "<< file << ":" << lineno;
    //const char* msg = _DTrait<Exception>::decorate(oss);
    //DR_ERROR(msg);
    //throw Exception(msg);
  //}
/*};*/

#define _dr_debug_throw_exception(oss, exctype) {\
    std::string str = oss.str();\
    DR_ERROR(str.c_str());\
    throw exctype(str.c_str());\
  }
#define _dr_debug_throw_detail_exception(oss, exctype) {\
    oss << ", "<< __FILE__ << ":" << __LINE__;\
    _dr_debug_throw_exception(oss, exctype);\
  }

#define _dr_throw_exception(oss, exctype) {\
    std::string str = oss.str();\
    throw exctype(str.c_str());\
  }
#define _dr_throw_detail_exception(oss, exctype) {\
    oss << ", "<< __FILE__ << ":" << __LINE__;\
    _dr_throw_exception(oss, exctype);\
  }

#define dr_throw_exception(ossexpr) {\
    std::ostringstream oss;\
    oss << ossexpr;\
    _dr_debug_throw_detail_exception(oss, dr_exception);\
  }

struct uv_guard {
    uv_guard(uv_mutex_t& mutex) : ptr_(&mutex) {
        uv_mutex_lock(ptr_);
    }

    void guard() {
        if(ptr_) {
            uv_mutex_lock(ptr_);
        }
    }

    void release() {
        if(ptr_) {
            uv_mutex_unlock(ptr_);
            ptr_ = NULL;
        }
    }

    ~uv_guard() {
        if(ptr_) {
            uv_mutex_unlock(ptr_);
        }
    }

    uv_mutex_t* ptr_;
};

struct MediaFileConfig {
};

typedef shared_ptr<webrtc::NetEq> NetEqPtr;
typedef std::list<NetEqPtr> NetEqList;
typedef shared_ptr<webrtc::AudioFrame> FramePtr;
typedef std::list<FramePtr> FrameList;



class Channel {
public:
  Channel(uint8_t id, const webrtc::NetEq::Config& config)
    : eq_(webrtc::NetEq::Create(config))
    , received_timestamp_(0)
    , first_timestamp_(0)
    , first_packet_(true)
    , id_(id) {
      RegisterPayloadTypes();
  }

  NetEqPtr GetEQ() {
    return eq_;
  }

  uint8_t id() const {
    return id_;
  }

  void InsertPacket(const webrtc::WebRtcRTPHeader& rtp_header, const uint8_t * data, size_t len) {
    if(first_packet_) {
        first_packet_ = false;
        first_timestamp_ = rtp_header.header.timestamp;
        DR_DEBUG("chan %02x received first packet", id_);
    } else {
        if(rtp_header.header.timestamp < first_timestamp_) {
          dr_throw_exception("[DRecorder] InsertPacket failed because timestamp discard! " << 
                  rtp_header.header.timestamp << " vs " << first_timestamp_);
        }
    }
    //received_timestamp_ = webrtc::Clock::GetRealTimeClock()->TimeInMilliseconds();
    if(webrtc::NetEq::kOK != eq_->InsertPacket(rtp_header, data, len, received_timestamp_)) {
      dr_throw_exception("[DRecorder] InsertPacket failed! " << received_timestamp_);
    }
    received_timestamp_ += len;
  }

  uint64_t timestamp() const {
    return received_timestamp_;
  }

  //adjust MTG packet timestamp to WebRTC RTP timestamp spec.
  uint32_t adjust_timestamp(const webrtc::WebRtcRTPHeader& rtp_header) {
      if(!first_packet_) {
          if(rtp_header.header.timestamp >= first_timestamp_) {
              return webrtc_packet_timestamp<MTG_PAYLOAD_LEN, MTG_PTIME>(first_timestamp_, rtp_header.header.timestamp);
          }
      }
      return rtp_header.header.timestamp;
  }
  
private:
  void RegisterPayloadTypes() {
    int error = eq_->RegisterPayloadType(webrtc::kDecoderPCMa, MTG_PAYLOAD_TYPE);
    if (error) {
      dr_throw_exception("Cannot register payload type " << MTG_PAYLOAD_TYPE << " as a-law");
    }
  }
private:
  NetEqPtr eq_;
  uint64_t received_timestamp_;
  uint32_t first_timestamp_;
  bool first_packet_;
  uint8_t id_;
};

typedef shared_ptr<Channel> ChannelPtr;
typedef std::list<ChannelPtr> ChannelList;

static uint32_t g_instance = 0;
uint32_t acquire_instance_id() {
  return g_instance++;
}

class Recorder : public webrtc::FileCallback {
public:
    Recorder(const std::string& session_id, const std::string& caller, const std::string& callee, const std::string& out_path
            , const std::string& fmt)
        : id_(session_id)
        , caller_(caller)
        , callee_(callee)
        , out_path_(out_path)
        , fmt_(fmt)
        , recording_(false) 
        , media_file_(NULL)
        , instance_id_(acquire_instance_id()) {
      Validate();
      uv_mutex_init(&lock_);
    }

    ~Recorder() {
      close();
      webrtc::MediaFile::DestroyMediaFile(media_file_);
      uv_mutex_destroy(&lock_);
      DR_DEBUG("[DRecord] recorder destroyed: (%s)%s", id_.c_str(), out_path_.c_str());
    }

    const std::string& id() const {
      return id_;
    }

    void close() {
      if(media_file_) {
        media_file_->StopRecording();
      }
    }

    uint8_t AddChannel(const webrtc::NetEq::Config& config) {
      uv_guard guard(lock_);
      uint8_t chan_id = channel_list_.size();
      channel_list_.push_back(ChannelPtr(new Channel(chan_id, config)));

      return chan_id;
    }

    ChannelPtr GetChannel(uint8_t chan_id) {
      uv_guard guard(lock_);
      for(ChannelList::iterator it = channel_list_.begin(); it != channel_list_.end(); ++it) {
        if((*it)->id() == chan_id) {
          return *it;
        }
      }
      guard.release();
      dr_throw_exception("not found channel " << chan_id << " at session " << id_);
    }

    /*NetEqList GetAllEQ() {*/
      //NetEqList eqlist;
      //ssize_t size = 0;
      //{
        //uv_guard guard(lock_);
        //size = channel_list_.size();
      //}
      //uv_guard guard(lock_);
      //for(ChannelList::iterator it = channel_list_.begin(); it != channel_list_.end(); ++it) {
        //eqlist.push(it->GetEQ());
      //}
      //guard.release();
      //return eqlist;
    /*}*/

    ChannelList GetAllChannels() {
        uv_guard guard(lock_);
        return channel_list_;
    }

    webrtc::MediaFile* GetMediaFile() {
      return media_file_;
    }

    bool IsRecording() const {
      return recording_ || (media_file_ && media_file_->IsRecording());
    }

    void StartRecording() {
      if(!recording_) {
        CreateMediaFile();
      }
    }

    void IncomingData(uint8_t chan_id, const char* data, size_t len) {
      ChannelPtr channel = GetChannel(chan_id);
      if(!channel) {
        dr_throw_exception("[DRecorder] InsertPacket can't found chan_id " << (uint32_t)chan_id);
      }
      mtg_rtp_g711* mtg_rtp = NULL;
      webrtc::WebRtcRTPHeader rtp_header;
      ParseData(data, len, rtp_header, &mtg_rtp);
      DR_DEBUG("%s InsertPacket to NetEq: chan 0x%X, rtp => ts:0x%04X, sn:0x%02X, received ts:0x%04X"
          , id_.c_str(), chan_id, rtp_header.header.timestamp, rtp_header.header.sequenceNumber, channel->timestamp());
      rtp_header.header.timestamp = channel->adjust_timestamp(rtp_header);
      channel->InsertPacket(rtp_header, mtg_rtp->payload, MTG_PAYLOAD_LEN);
    }

    void record(uint64_t* ga, uint64_t* mix, uint64_t* write, uint64_t* rtime) {
      uint64_t begin_ts = uv_hrtime();
      uint64_t ga_ts = 0, ga_diff_total = 0;
      uint64_t alloc_ts = 0, alloc_diff_total = 0;
      ChannelList channel_list = GetAllChannels();
      if(channel_list.empty()) {
        dr_throw_exception("[DRecorder] record can't found any channels");
      }

      FrameList frames;
      for(ChannelList::iterator it = channel_list.begin(); it != channel_list.end(); ++it) {
        NetEqPtr eq = (*it)->GetEQ();
        alloc_ts = uv_hrtime();
        FramePtr frame(new webrtc::AudioFrame);
        ga_ts = uv_hrtime();
        alloc_diff_total += ga_ts - alloc_ts;
        int result = eq->GetAudio(webrtc::AudioFrame::kMaxDataSizeSamples, frame->data_
              ,&frame->samples_per_channel_, &frame->num_channels_, NULL);
        ga_diff_total += uv_hrtime() - ga_ts;

        if(webrtc::NetEq::kOK == result) {
          frames.push_back(frame);
        } else {
          DR_ERROR("[DRecorder] GetAudio failed of channel %d", (*it)->id());
        }
      }

      if(ga) {
          *ga = ga_diff_total/channel_list.size();
      }

      if(frames.empty()) {
        dr_throw_exception("[DRecorder] can't got frames when receive");
      }
      webrtc::AudioFrame mixed_frame;
      uint64_t mix_ts = uv_hrtime();
      MixAudio(frames, mixed_frame);
      if(!IsRecording()) {
        StartRecording();
      }
      uint64_t write_ts = uv_hrtime();
      GetMediaFile()->IncomingAudioData((int8_t*)mixed_frame.data_, mixed_frame.samples_per_channel_*2);
      uint64_t end_ts = uv_hrtime();
      if(mix) {
          *mix = write_ts - mix_ts;
      }
      if(write) {
          *write = end_ts - write_ts;
      }
      if(rtime) {
        *rtime = end_ts-begin_ts;
      }
      DR_ERROR("record cosumed %d ns, write consumed %d ns, mix consumed %d, get_audio %d, alloc %d", end_ts-begin_ts, end_ts-write_ts, write_ts-mix_ts, ga_diff_total/channel_list.size(), alloc_diff_total/channel_list.size());
    }

protected: //webrtc callback
    // This function is called by MediaFile when a file has been playing for
    // durationMs ms. id is the identifier for the MediaFile instance calling
    // the callback.
    virtual void PlayNotification(const int32_t id,
                                  const uint32_t durationMs) {};

    // This function is called by MediaFile when a file has been recording for
    // durationMs ms. id is the identifier for the MediaFile instance calling
    // the callback.
    virtual void RecordNotification(const int32_t id,
                                    const uint32_t durationMs) {
      DR_DEBUG("[DRecord] recording %d ms at %d", durationMs, id);
    };

    // This function is called by MediaFile when a file has been stopped
    // playing. id is the identifier for the MediaFile instance calling the
    // callback.
    virtual void PlayFileEnded(const int32_t id) {};

    // This function is called by MediaFile when a file has been stopped
    // recording. id is the identifier for the MediaFile instance calling the
    // callback.
    virtual void RecordFileEnded(const int32_t id) {
      DR_DEBUG("[DRecord] recording stop : (%d)%s", id, out_path_.c_str());
    };

private:
    void Validate() {
        if( fmt_ != "wav" && fmt_ != "pcm" ) {
           dr_throw_exception("[DRecord] unsupport record file format: " << fmt_);
        }
    }

    void CreateMediaFile() {
      media_file_ = webrtc::MediaFile::CreateMediaFile(instance_id_);
      media_file_->SetModuleFileCallback(this);
      webrtc::CodecInst codec_inst = {8,"L16",webrtc::kFreq8000Hz,80,1, 16000};

      int result = -1;
      if(fmt_ == "wav") {
         result = media_file_->StartRecordingAudioFile(out_path_.c_str(), webrtc::kFileFormatWavFile, codec_inst, 1000);
      } else if(fmt_ == "pcm") {
          result = media_file_->StartRecordingAudioFile(out_path_.c_str(), webrtc::kFileFormatPcm8kHzFile, codec_inst, 1000);
      }
      if(-1 == result) {
        dr_throw_exception("[DRecord] mediafile StartRecordingAudioFile");
      } else {
        recording_ = media_file_->IsRecording();
      }
    }

    void ParseData(const char* buffer, size_t buffer_size, webrtc::WebRtcRTPHeader& rtp_header, mtg_rtp_g711** mtg_rtp) {
      if(buffer_size != sizeof(mtg_rtp_g711)) {
        dr_throw_exception("[DRecorder] payload data size not equal than g711 spec:"<<(uint32_t)buffer_size<<"!="<<(uint32_t)sizeof(mtg_rtp_g711));
      }
      *mtg_rtp = (mtg_rtp_g711*)buffer;
      uint32_t magic = (*mtg_rtp)->magic;
      if(magic != MTG_PAYLOAD_MAGIC_1 && magic != MTG_PAYLOAD_MAGIC_2) { //direct compare with net order, magic real value is 0x000000ff
        dr_throw_exception("[DRecorder] invalid magic of g711 payload "<<magic);
      }

      rtp_header.header.sequenceNumber = read_le16((uint8_t*)&((*mtg_rtp)->seq))/2; //must divde 2, because MTG sequence number gap 2
      rtp_header.header.timestamp = read_le16(((uint8_t*)&((*mtg_rtp)->timestamp))) << 16 | read_le16(((uint8_t*)&((*mtg_rtp)->timestamp)+2));
      rtp_header.header.ssrc = 0;
      rtp_header.header.markerBit = false;
      rtp_header.header.payloadType = MTG_PAYLOAD_TYPE;
      rtp_header.header.numCSRCs = 0;
      rtp_header.frameType = webrtc::kAudioFrameSpeech;
    }

    void MixAudio(const FrameList& frames, webrtc::AudioFrame& mixed_frame) {
      FrameList::const_iterator it = frames.begin();
      mixed_frame.CopyFrom(**it);
      for(++it; it != frames.end(); ++it) {
        if(mixed_frame.samples_per_channel_ != (*it)->samples_per_channel_) {
          DR_ERROR("[DRecorder] samples_per_channel mismatch when mix:%d vs %d", mixed_frame.samples_per_channel_, (*it)->samples_per_channel_);
        }
        if(!mixed_frame.samples_per_channel_) {
          DR_ERROR("[DRecorder] samples_per");
        }
        MixAudio(mixed_frame, **it);
      }
    }

    void MixAudio(webrtc::AudioFrame& mixed_frame, const webrtc::AudioFrame& rhs) {
      DR_ERROR("[DRecorder] samples_per_channel %d, %d", mixed_frame.samples_per_channel_, rhs.samples_per_channel_);
      int samples_per_channel  = 
        (mixed_frame.samples_per_channel_ == 0 || mixed_frame.samples_per_channel_ > rhs.samples_per_channel_) ? 
          rhs.samples_per_channel_ : mixed_frame.samples_per_channel_;
      for(int i = 0; i < samples_per_channel; ++i) {
        mixed_frame.data_[i] = (mixed_frame.data_[i] + rhs.data_[i]) >> 1;
      }
    }

private:
    std::string id_;
    std::string caller_;
    std::string callee_;
    std::string out_path_;
    std::string fmt_;
    bool recording_;
    ChannelList channel_list_;
    webrtc::MediaFile* media_file_;
    uv_mutex_t lock_;
    int32_t instance_id_;
};

typedef shared_ptr<Recorder> RecorderPtr;
typedef unordered_map<std::string, RecorderPtr> RecorderMap;
RecorderMap g_recorders;

class RuntimeStats {
public:
    RuntimeStats() 
        : total_ga_consume_(0)
        , total_mix_consume_(0)
        , total_write_consume_(0)
        , total_record_consume_(0)
        , count_(0)
        , begin_time_(uv_hrtime()) {
    }

    ~RuntimeStats() {
    }

    void append_samples(uint64_t ga, uint64_t mix, uint64_t write, uint64_t record) {
        total_ga_consume_ += ga;
        total_mix_consume_ += mix;
        total_write_consume_ += write;
        total_record_consume_ += record;
        count_++;
    }

    void avg_values(uint64_t* ga, uint64_t* mix, uint64_t* write, uint64_t* record) {
      if(count_) {
        *ga = total_ga_consume_/count_;
        *mix = total_mix_consume_/count_;
        *write = total_write_consume_/count_;
        *record = total_record_consume_/count_;
      } else {
        *ga = 0;
        *mix = 0;
        *write = 0;
        *record = 0;
      }
    }

    void discard() {
      total_ga_consume_ = 0;
      total_mix_consume_ = 0;
      total_write_consume_ = 0;
      total_record_consume_ = 0;
      count_ = 0;
      begin_time_ = uv_hrtime();
    }

private:
    uint64_t total_ga_consume_;
    uint64_t total_mix_consume_;
    uint64_t total_write_consume_;
    uint64_t total_record_consume_;
    uint64_t count_;
    uint64_t begin_time_;
};

typedef std::shared_ptr<RuntimeStats> RuntimeStatsPtr;
RuntimeStatsPtr g_runtime_stats(new RuntimeStats);

RecorderMap::const_iterator check_recorder_not_exists(const char* session_id) {
  RecorderMap::const_iterator it = g_recorders.find(session_id);
  if(it != g_recorders.end()) {
    dr_throw_exception("[DRecorder] recorder " << session_id << " already exist!");
  }
  return it;
}

RecorderMap::const_iterator check_recorder_exists(const char* session_id) {
  RecorderMap::const_iterator it = g_recorders.find(session_id);
  if(it == g_recorders.end()) {
    dr_throw_exception("[DRecorder] recorder " << session_id << " not exist!");
  }
  return it;
}


class RecordWorker : public NanAsyncWorker {
public:
  RecordWorker(NanCallback *callback, RecorderPtr recorder) 
    : NanAsyncWorker(callback)
    , recorder_(recorder) {
  }
  ~RecordWorker() {}

  void Execute () {
    try {
      if(!recorder_) {
        dr_throw_exception("[DRecorder] record can't found recorder");
      }

      recorder_->record(&ga_consume_, &mix_consume_, &write_consume_, &record_consume_);

    } catch(const v8::Exception& ex) {
      SetErrorMessage("[DRecorder] InsertPakcet got v8 Exception");
      return;
    } catch(const std::exception& ex) {
      SetErrorMessage(ex.what());
      return;
    }

  }

  virtual void HandleOKCallback () {
      NanScope();

      Local<Value> argv[] = {
          NanNull()
      };
      g_runtime_stats->append_samples(ga_consume_, mix_consume_, write_consume_, record_consume_);
      callback->Call(1, argv);
  };

private:
  uint64_t ga_consume_;
  uint64_t mix_consume_;
  uint64_t write_consume_;
  uint64_t record_consume_;
  RecorderPtr recorder_;
};

class InsertPacketWorker : public NanAsyncWorker {
public:
  InsertPacketWorker(NanCallback *callback, RecorderPtr recorder, uint8_t chan_id, const Local<Value>& buffer) 
    : NanAsyncWorker(callback)
    , recorder_(recorder)
    , chan_id_(chan_id)
    {
      NanScope();
      NanAssignPersistent(buffer_p_, Local<Object>::Cast(buffer));
      Local<Object> obj = NanNew<Object>(buffer_p_);
      buffer_size_ = node::Buffer::Length(obj);
      buffer_ = node::Buffer::Data(obj);
      if(!buffer_ || buffer_size_ == 0) {
        dr_throw_exception("[DRecorder] invalid buffer for store payload data");
      }
  }
  ~InsertPacketWorker() {
    NanDisposePersistent(buffer_p_);
  }

  void Execute () {
    try {
      if(!recorder_) {
        dr_throw_exception("[DRecorder] InsertPacket can't found recorder");
      }
      recorder_->IncomingData(chan_id_, buffer_, buffer_size_);
    } catch(const v8::Exception& ex) {
      SetErrorMessage("[DRecorder] InsertPakcet got v8 Exception");
      return;
    } catch(const std::exception& ex) {
      SetErrorMessage(ex.what());
      return;
    }
  }

  virtual void HandleOKCallback () {
      NanScope();

      Local<Value> argv[] = {
         NanNull() 
      };
      callback->Call(1, argv);
  };

private:
  RecorderPtr recorder_;
  uint8_t chan_id_;
  v8::Persistent<v8::Object> buffer_p_;
  char* buffer_;
  ssize_t buffer_size_;
};


///////////////////////////////////////////////////////////////////////////////
// init -- init runtime env
// @params
//   trace_file :: string() -- trace file fullpath .
// @return
//   success: undefined.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(init) {
  NanScope();

  /*if (args.Length() < 1) {*/
    //ThrowException(Exception::TypeError(String::New("Wrong number of arguments")));
    //NanReturnUndefined();
  /*}*/

  if (args.Length() > 0 && !args[0]->IsString()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  if (args.Length() > 1 && !args[1]->IsUint32()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  WebRtcSpl_Init();

  //init webrtc trace file
  if(args.Length() > 0) {
    webrtc::Trace::CreateTrace();
    webrtc::Trace::SetTraceFile(*NanUtf8String(args[0]), true);
    if (args.Length() > 1) {
      webrtc::Trace::set_level_filter((webrtc::TraceLevel)args[1]->ToUint32()->Value());
    } else {
      webrtc::Trace::set_level_filter(webrtc::kTraceAll);
    }
  }

  NanReturnUndefined();
}

void string_type_check(Handle<v8::String> key, Handle<Value> value) {
  if(!value->IsString()) {
    dr_throw_exception("[DRecorder] key '" << *v8::String::Utf8Value(key) << "' value is not a string");
  }
}

Handle<String> object_get_str(Handle<Object> obj, Handle<v8::String> key) {
  Handle<Value> result = obj->Get(key);
  string_type_check(key, result);
  return Handle<String>::Cast(result);
}

template<class Type>
Handle<Type> object_get_default(Handle<Object> obj, Handle<v8::String> key, Handle<Type> default_value) {
  Handle<Value> result = Handle<Type>::Cast(obj->Get(key));
  return result->IsUndefined() ? default_value : result;
}

uint32_t object_get_uint32(Handle<Object> obj, Handle<v8::String> key, uint32_t default_value) {
  Handle<Value> result = obj->Get(key);
  return result->IsUndefined() ? default_value : result->Uint32Value();
}

int32_t object_get_int32(Handle<Object> obj, Handle<v8::String> key, int32_t default_value) {
  Handle<Value> result = obj->Get(key);
  return result->IsUndefined() ? default_value : result->Int32Value();
}

int64_t object_get_integer(Handle<Object> obj, Handle<v8::String> key, int64_t default_value) {
  Handle<Value> result = obj->Get(key);
  return result->IsUndefined() ? default_value : result->IntegerValue();
}

double object_get_double(Handle<Object> obj, Handle<v8::String> key, double default_value) {
  Handle<Value> result = obj->Get(key);
  return result->IsUndefined() ? default_value : result->NumberValue();
}

bool object_get_bool(Handle<Object> obj, Handle<v8::String> key, bool default_value) {
  Handle<Value> result = obj->Get(key);
  return result->IsUndefined() ? default_value : result->BooleanValue();
}

void object_to_neteq_config(Handle<Object> obj, webrtc::NetEq::Config& config) {
  webrtc::NetEq::Config eq_config;
  Local<String> key_sample_rate_hz = String::New("sample_rate_hz");
  Local<String> key_enable_audio_classifier = String::New("enable_audio_classifier");
  Local<String> key_max_packets_in_buffer = String::New("max_packets_in_buffer");
  Local<String> key_max_delay_ms = String::New("max_delay_ms");
  Local<String> key_background_noise_mode = String::New("background_noise_mode");
  Local<String> key_playout_mode = String::New("playout_mode");
  int32_t sample_rate_hz = object_get_int32(obj, key_sample_rate_hz, 16000);
  bool enable_audio_classifier = object_get_bool(obj, key_enable_audio_classifier, false);
  int32_t max_packets_in_buffer = object_get_int32(obj, key_max_packets_in_buffer, 50);
  int32_t max_delay_ms = object_get_int32(obj, key_max_delay_ms, 2000);
  int32_t background_noise_mode = object_get_int32(obj, key_background_noise_mode, webrtc::NetEq::kBgnOn);
  int32_t playout_mode = object_get_int32(obj, key_playout_mode, webrtc::kPlayoutOn);
  config.sample_rate_hz = sample_rate_hz;
  config.enable_audio_classifier = enable_audio_classifier;
  config.max_packets_in_buffer = max_packets_in_buffer;
  config.max_delay_ms = max_delay_ms;
  config.background_noise_mode = (webrtc::NetEq::BackgroundNoiseMode)background_noise_mode;
  config.playout_mode = (webrtc::NetEqPlayoutMode)playout_mode;
}


///////////////////////////////////////////////////////////////////////////////
// create recorder -- create recorder and create it's channels.
// @params
//   session_id :: string() -- <prefix>_<callid>, prefix should combine with 
//       peer_ip/timestamp.
//   caller :: string() -- caller number.
//   called :: string() -- called number.
//   config :: object() -- config object
//   - out_dir :: string() -- directory of audio file save.
//   - format :: string() -- file format, only support 'wav' now.
// @return
//   success: undefined.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(start_recorder) {
  NanScope();

  if (args.Length() < 4) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString() || !args[1]->IsString() || !args[2]->IsString() || !args[3]->IsObject()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }


  try {
    v8::String::AsciiValue session_id(args[0]->ToString());
    v8::String::AsciiValue caller(args[1]->ToString());
    v8::String::AsciiValue called(args[2]->ToString());
    v8::Local<Object> config(args[3]->ToObject());
    v8::String::AsciiValue out_put(object_get_str(config, String::New("out_dir")));
    v8::String::AsciiValue fmt(object_get_str(config, String::New("format")));
    RecorderMap::const_iterator it = check_recorder_not_exists(*session_id);
    RecorderPtr recorder(new Recorder(*session_id, *caller, *called, *out_put, *fmt));
    g_recorders.insert(it, RecorderMap::value_type(recorder->id(), recorder));
    DR_DEBUG("start drecorder:%s", *session_id);
  } catch(const std::exception& ex) {
    NanThrowTypeError(ex.what());
    NanReturnUndefined();
  }

  NanReturnUndefined();
}


///////////////////////////////////////////////////////////////////////////////
// stop recorder
// @params
//   session_id :: string() -- id of recorder.
// @return
//   success: undefined.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(stop_recorder) {
  NanScope();

  if (args.Length() < 1) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  try {
    v8::String::AsciiValue session_id(args[0]->ToString());
    RecorderMap::iterator it = g_recorders.find(std::string(*session_id));
    if(it != g_recorders.end() && it->second) {
      it->second->close();
      g_recorders.erase(it);
    }

    DR_DEBUG("stop recorder:%s, now only have %d recorders", *session_id, g_recorders.size());
  } catch(const std::exception& ex) {
    NanThrowTypeError(ex.what());
    NanReturnUndefined();
  }

  NanReturnUndefined();
}


///////////////////////////////////////////////////////////////////////////////
// add channels to record
// @params
//   session_id :: string() -- recorder session id.
//   count :: integer() -- count of channels.
//   config :: object() -- NetEq config object.
//   - sample_rate_hz :: integer() -- default 16000.
//   - enable_audio_classifier :: boolean() -- default false.
//   - max_packets_in_buffer :: integer() -- default 50.
//   - max_delay_ms :: integer() -- default 2000.
//   - background_noise_mode :: boolean() -- default kBgnOff.
//   - playout_mode :: boolean() -- default kPlayoutOn.
// @return
//   success: array of channels id.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(add_channels) {
  NanScope();

  if (args.Length() < 3) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString() || !args[1]->IsNumber() || !args[2]->IsObject()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  try {
    v8::String::AsciiValue session_id(args[0]->ToString());
    uint32_t count(args[1]->Uint32Value());
    if(count <= 0) {
      throw std::invalid_argument("[DRecorder] channel count can't be 0!");
    }
    v8::Local<v8::Object> config_obj(args[2]->ToObject());
    webrtc::NetEq::Config eq_config;
    object_to_neteq_config(config_obj, eq_config);
    RecorderPtr recorder = g_recorders.at(*session_id);
    Handle<Array> result = Array::New(count);
    DR_DEBUG("add recorder %s channels", recorder->id().c_str());
    for(uint32_t i = 0; i < count; ++i) {
      uint8_t chan_id = recorder->AddChannel(eq_config);
      result->Set(i, Integer::New((int32_t)chan_id));
    }
    NanReturnValue(result);
  } catch(const std::exception& ex) {
    NanThrowTypeError(ex.what());
    NanReturnUndefined();
  }
}

///////////////////////////////////////////////////////////////////////////////
// record -- do record per 10ms. until complete other next record not fire.
// @params
//   session_id :: string() -- recorder session id.
//   callback :: function() -- callback action
//   -- callback prototype :: void callback(error).
// @return
//   success: undefined
//   failed: throw v8 exception
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(record) {
  NanScope();

  if (args.Length() < 2) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString() || !args[1]->IsFunction()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  NanCallback *callback = new NanCallback(args[1].As<Function>());
  v8::String::AsciiValue session_id(args[0]->ToString());
  RecorderPtr recorder;

  try {
    recorder = g_recorders.at(*session_id);
  } catch(const std::out_of_range& ex) {
    //session not exists, throw to javascript use callback
  }

  NanAsyncQueueWorker(new RecordWorker(callback, recorder));
  NanReturnUndefined();
}

///////////////////////////////////////////////////////////////////////////////
// channel_on_data -- channel received data. insert packet async. multi worker
//      can work concurrent.
// @params
//   session_id :: string() -- recorder session id.
//   chan_id :: integer() -- channel id in recorder.
//   data :: buffer() -- received data
//   callback :: function() -- callback action.
//   -- callback prototype :: void callback(error).
// @return
//   success: undefined.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(channel_on_data) {
  NanScope();

  if (args.Length() < 3) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString() || !args[1]->IsUint32() || !args[2]->IsObject() || !node::Buffer::HasInstance(args[2]) 
      || !args[3]->IsFunction()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  v8::String::AsciiValue session_id(args[0]->ToString());
  uint8_t chan_id(args[1]->Uint32Value());

  RecorderPtr recorder;
  try {
    recorder = g_recorders.at(*session_id);
  } catch(const std::out_of_range& ex) {
    //session not exists, throw to javascript use callback
  }

  NanCallback *callback = new NanCallback(args[3].As<Function>());
  //LOG(INFO) << "[EasyWiFi] change auth async " << *SSID << ", " << *passwd;
  NanAsyncQueueWorker(new InsertPacketWorker(callback, recorder, chan_id, args[2]));
  //LOG(INFO) << "start ap async " << SSID;
  NanReturnUndefined();
}

///////////////////////////////////////////////////////////////////////////////
// record_sync -- do record per 10ms. until complete other next record not fire.
// @params
//   session_id :: string() -- recorder session id.
//   -- callback prototype :: void callback(error).
// @return
//   success: undefined
//   failed: throw v8 exception
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(record_sync) {
  NanScope();

  if (args.Length() < 1) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString()) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  v8::String::AsciiValue session_id(args[0]->ToString());
  RecorderPtr recorder;

  DR_DEBUG("call once recording");
  try {
    uint64_t ga;
    uint64_t mix;
    uint64_t write;
    uint64_t record;
    recorder = g_recorders.at(*session_id);
    recorder->record(&ga, &mix, &write, &record);
    g_runtime_stats->append_samples(ga, mix, write, record);
  } catch(const std::exception& ex) {
    //session not exists, throw to javascript use callback
    NanThrowTypeError(ex.what());
  }

  DR_DEBUG("call end recording");

  NanReturnUndefined();
}

///////////////////////////////////////////////////////////////////////////////
// channel_on_data_sync -- channel received data. insert packet async. multi worker
//      can work concurrent.
// @params
//   session_id :: string() -- recorder session id.
//   chan_id :: integer() -- channel id in recorder.
//   data :: buffer() -- received data
// @return
//   success: undefined.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(channel_on_data_sync) {
  NanScope();

  if (args.Length() < 3) {
    NanThrowTypeError("Wrong number of arguments");
    NanReturnUndefined();
  }

  if (!args[0]->IsString() || !args[1]->IsUint32() || !args[2]->IsObject() || !node::Buffer::HasInstance(args[2])) {
    NanThrowTypeError("Wrong arguments");
    NanReturnUndefined();
  }

  v8::String::AsciiValue session_id(args[0]->ToString());
  uint8_t chan_id(args[1]->Uint32Value());

  RecorderPtr recorder;
  try {
    recorder = g_recorders.at(*session_id);
    recorder->IncomingData(chan_id, node::Buffer::Data(args[2]), node::Buffer::Length(args[2]));
  } catch(const std::exception& ex) {
    //session not exists, throw to javascript use callback
    NanThrowTypeError(ex.what());
  }

  NanReturnUndefined();
}


///////////////////////////////////////////////////////////////////////////////
//net_stats_to_obj
/*struct NetEqNetworkStatistics {*/
  //uint16_t current_buffer_size_ms;  // Current jitter buffer size in ms.
  //uint16_t preferred_buffer_size_ms;  // Target buffer size in ms.
  //uint16_t jitter_peaks_found;  // 1 if adding extra delay due to peaky
                                //// jitter; 0 otherwise.
  //uint16_t packet_loss_rate;  // Loss rate (network + late) in Q14.
  //uint16_t packet_discard_rate;  // Late loss rate in Q14.
  //uint16_t expand_rate;  // Fraction (of original stream) of synthesized
                         //// speech inserted through expansion (in Q14).
  //uint16_t preemptive_rate;  // Fraction of data inserted through pre-emptive
                             //// expansion (in Q14).
  //uint16_t accelerate_rate;  // Fraction of data removed through acceleration
                             //// (in Q14).
  //int32_t clockdrift_ppm;  // Average clock-drift in parts-per-million
                           //// (positive or negative).
  //int added_zero_samples;  // Number of zero samples added in "off" mode.
/*};*/
///////////////////////////////////////////////////////////////////////////////
void net_stats_to_obj(const webrtc::NetEqNetworkStatistics& net_stats, v8::Local<v8::Object>& obj) {
  obj->Set(NanNew<String>("current_buffer_size_ms"), NanNew<v8::Uint32>((uint32_t)net_stats.current_buffer_size_ms));
  obj->Set(NanNew<String>("preferred_buffer_size_ms"), NanNew<v8::Uint32>((uint32_t)net_stats.preferred_buffer_size_ms));
  obj->Set(NanNew<String>("jitter_peaks_found"), NanNew<v8::Uint32>((uint32_t)net_stats.jitter_peaks_found));
  obj->Set(NanNew<String>("packet_loss_rate"), NanNew<v8::Uint32>((uint32_t)net_stats.packet_loss_rate));
  obj->Set(NanNew<String>("packet_discard_rate"), NanNew<v8::Uint32>((uint32_t)net_stats.packet_discard_rate));
  obj->Set(NanNew<String>("expand_rate"), NanNew<v8::Uint32>((uint32_t)net_stats.expand_rate));
  obj->Set(NanNew<String>("preemptive_rate"), NanNew<v8::Uint32>((uint32_t)net_stats.preemptive_rate));
  obj->Set(NanNew<String>("accelerate_rate"), NanNew<v8::Uint32>((uint32_t)net_stats.accelerate_rate));
  obj->Set(NanNew<String>("clockdrift_ppm"), NanNew<v8::Int32>(net_stats.clockdrift_ppm));
  obj->Set(NanNew<String>("added_zero_samples"), NanNew<v8::Int32>(net_stats.added_zero_samples));
}


///////////////////////////////////////////////////////////////////////////////
//net_stats_to_obj
/*struct RtcpStatistics {*/
  //RtcpStatistics()
    //: fraction_lost(0),
      //cumulative_lost(0),
      //extended_max_sequence_number(0),
      //jitter(0) {}

  //uint8_t fraction_lost;
  //uint32_t cumulative_lost;
  //uint32_t extended_max_sequence_number;
  //uint32_t jitter;
/*};*/
///////////////////////////////////////////////////////////////////////////////
void rtcp_stats_to_obj(const webrtc::RtcpStatistics& rtcp_stats, v8::Local<v8::Object>& obj) {
  obj->Set(NanNew<String>("fraction_lost"), NanNew<v8::Uint32>((uint32_t)rtcp_stats.fraction_lost));
  obj->Set(NanNew<String>("cumulative_lost"), NanNew<v8::Uint32>(rtcp_stats.cumulative_lost));
  obj->Set(NanNew<String>("extended_max_sequence_number"), NanNew<v8::Uint32>(rtcp_stats.extended_max_sequence_number));
  obj->Set(NanNew<String>("jitter"), NanNew<v8::Uint32>(rtcp_stats.jitter));
}

struct AvgNetEqNetworkStatistics {
  AvgNetEqNetworkStatistics()
    : current_buffer_size_ms(0)
    , preferred_buffer_size_ms(0)
    , jitter_peaks_found(0)
    , packet_loss_rate(0)
    , packet_discard_rate(0)
    , expand_rate(0)
    , preemptive_rate(0)
    , accelerate_rate(0)
    , clockdrift_ppm(0)
    , added_zero_samples(0) {
  }

  uint32_t current_buffer_size_ms;  // Current jitter buffer size in ms.
  uint32_t preferred_buffer_size_ms;  // Target buffer size in ms.
  uint32_t jitter_peaks_found;  // 1 if adding extra delay due to peaky
                                // jitter; 0 otherwise.
  uint32_t packet_loss_rate;  // Loss rate (network + late) in Q14.
  uint32_t packet_discard_rate;  // Late loss rate in Q14.
  uint32_t expand_rate;  // Fraction (of original stream) of synthesized
                         // speech inserted through expansion (in Q14).
  uint32_t preemptive_rate;  // Fraction of data inserted through pre-emptive
                             // expansion (in Q14).
  uint32_t accelerate_rate;  // Fraction of data removed through acceleration
                             // (in Q14).
  int32_t clockdrift_ppm;  // Average clock-drift in parts-per-million
                           // (positive or negative).
  int added_zero_samples;  // Number of zero samples added in "off" mode.
};

struct AvgRtcpStatistics {
  AvgRtcpStatistics()
    : fraction_lost(0),
      cumulative_lost(0),
      extended_max_sequence_number(0),
      jitter(0) {}

  uint32_t fraction_lost;
  uint32_t cumulative_lost;
  uint32_t extended_max_sequence_number;
  uint32_t jitter;
};


AvgRtcpStatistics& operator +=(AvgRtcpStatistics& lhs, const webrtc::RtcpStatistics& rhs) {
  lhs.fraction_lost += rhs.fraction_lost;
  lhs.cumulative_lost += rhs.cumulative_lost;
  lhs.extended_max_sequence_number += rhs.extended_max_sequence_number;
  lhs.jitter += rhs.jitter;
  return lhs;
}

AvgNetEqNetworkStatistics& operator +=(AvgNetEqNetworkStatistics& lhs, const webrtc::NetEqNetworkStatistics& rhs) {
  lhs.current_buffer_size_ms += rhs.current_buffer_size_ms;
  lhs.preferred_buffer_size_ms += rhs.preferred_buffer_size_ms;
  lhs.jitter_peaks_found += rhs.jitter_peaks_found;
  lhs.packet_loss_rate += rhs.packet_loss_rate;
  lhs.packet_discard_rate += rhs.packet_discard_rate;
  lhs.expand_rate += rhs.expand_rate;
  lhs.preemptive_rate += rhs.preemptive_rate;
  lhs.accelerate_rate += rhs.accelerate_rate;
  lhs.clockdrift_ppm += rhs.clockdrift_ppm;
  lhs.added_zero_samples += rhs.added_zero_samples;
  return lhs;
}

///////////////////////////////////////////////////////////////////////////////
// internal_status -- get all recorder stats, include network and rtcp stats.
// @params
//  NULL
// @return
//   success: array of channels id.
//   failed: throw v8 exception.
///////////////////////////////////////////////////////////////////////////////
NAN_METHOD(internal_status) {
  NanScope();

  //DR_DEBUG("internal_status count:%d", g_recorders.size());
  try {
    v8::Local<v8::Object> status_obj = NanNew<v8::Object>();

    v8::Local<v8::Object> rs_obj = NanNew<v8::Object>();
    size_t count = 0;
    AvgNetEqNetworkStatistics avg_net_stats;
    AvgRtcpStatistics avg_rtcp_stats;
    //for(RecorderMap::iterator it = g_recorders.begin(); it != g_recorders.end(); ++it) {
    for(auto& x: g_recorders) {
      ChannelList channels = x.second->GetAllChannels();
      if(channels.empty()) {
        continue;
      }
      v8::Local<v8::Object> cs_obj = NanNew<v8::Object>();
      rs_obj->Set(NanNew<v8::String, std::string>(x.first), cs_obj);
      for(ChannelList::iterator chan_it = channels.begin(); chan_it != channels.end(); ++chan_it) {
        NetEqPtr eq = (*chan_it)->GetEQ();
        webrtc::NetEqNetworkStatistics net_stats;
        webrtc::RtcpStatistics rtcp_stats;
        eq->NetworkStatistics(&net_stats);
        eq->GetRtcpStatisticsNoReset(&rtcp_stats);
        avg_net_stats += net_stats;
        avg_rtcp_stats += rtcp_stats;
        v8::Local<v8::Object> net_stats_obj = NanNew<v8::Object>();
        net_stats_to_obj(net_stats, net_stats_obj);
        v8::Local<v8::Object> rtcp_stats_obj = NanNew<v8::Object>();
        rtcp_stats_to_obj(rtcp_stats, rtcp_stats_obj);
        v8::Local<v8::Object> chan_obj = NanNew<v8::Object>();
        chan_obj->Set(NanNew<String>("net_stats"), net_stats_obj);
        chan_obj->Set(NanNew<String>("rtcp_stats"), rtcp_stats_obj);
        cs_obj->Set(NanNew<Uint32>((uint32_t)(*chan_it)->id()), chan_obj);
        count ++;
      }
    }

    v8::Local<v8::Object> rt_obj = NanNew<v8::Object>();
    uint64_t ga, mix, write, rtime;
    g_runtime_stats->avg_values(&ga, &mix, &write, &rtime);
    rt_obj->Set(NanNew<String>("audio_time"), NanNew<v8::Uint32>((uint32_t)ga));
    rt_obj->Set(NanNew<String>("mix_time"), NanNew<v8::Uint32>((uint32_t)mix));
    rt_obj->Set(NanNew<String>("write_time"), NanNew<v8::Uint32>((uint32_t)write));
    rt_obj->Set(NanNew<String>("record_time"), NanNew<v8::Uint32>((uint32_t)rtime));
    if(!count) {
      count = 1;
    } 

    rt_obj->Set(NanNew<String>("fraction_lost"), NanNew<v8::Uint32>((uint32_t)(avg_rtcp_stats.fraction_lost/count)));
    rt_obj->Set(NanNew<String>("cumulative_lost"), NanNew<v8::Uint32>((uint32_t)(avg_rtcp_stats.cumulative_lost/count)));
    rt_obj->Set(NanNew<String>("extended_max_sequence_number"), NanNew<v8::Uint32>((uint32_t)(avg_rtcp_stats.extended_max_sequence_number/count)));
    rt_obj->Set(NanNew<String>("jitter"), NanNew<v8::Uint32>((uint32_t)(avg_rtcp_stats.jitter/count)));
    rt_obj->Set(NanNew<String>("current_buffer_size_ms"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.current_buffer_size_ms/count)));
    rt_obj->Set(NanNew<String>("preferred_buffer_size_ms"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.preferred_buffer_size_ms/count)));
    rt_obj->Set(NanNew<String>("jitter_peaks_found"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.jitter_peaks_found/count)));
    rt_obj->Set(NanNew<String>("packet_loss_rate"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.packet_loss_rate/count)));
    rt_obj->Set(NanNew<String>("packet_discard_rate"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.packet_discard_rate/count)));
    rt_obj->Set(NanNew<String>("expand_rate"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.expand_rate/count)));
    rt_obj->Set(NanNew<String>("preemptive_rate"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.preemptive_rate/count)));
    rt_obj->Set(NanNew<String>("accelerate_rate"), NanNew<v8::Uint32>((uint32_t)(avg_net_stats.accelerate_rate/count)));
    rt_obj->Set(NanNew<String>("clockdrift_ppm"), NanNew<v8::Integer>(avg_net_stats.clockdrift_ppm/count));
    rt_obj->Set(NanNew<String>("added_zero_samples"), NanNew<v8::Integer>(avg_net_stats.added_zero_samples/count));

    g_runtime_stats->discard();

    status_obj->Set(NanNew<String>("sessions"), rs_obj);
    status_obj->Set(NanNew<String>("runtime"), rt_obj);

    NanReturnValue(status_obj);
  } catch(const std::exception& ex) {
    NanThrowTypeError(ex.what());
    NanReturnUndefined();
  }
}


void RegisterModule(Handle<Object> target) {
	
	//HMODULE handle = GetModuleHandle(NULL);
	//init_wifi_helper((HINSTANCE)handle);
	target->Set(String::NewSymbol("init"),
        FunctionTemplate::New(init)->GetFunction());
    target->Set(String::NewSymbol("start_recorder"),
        FunctionTemplate::New(start_recorder)->GetFunction());
	target->Set(String::NewSymbol("stop_recorder"),
        FunctionTemplate::New(stop_recorder)->GetFunction());
	target->Set(String::NewSymbol("add_channels"),
        FunctionTemplate::New(add_channels)->GetFunction());
	target->Set(String::NewSymbol("record"),
        FunctionTemplate::New(record)->GetFunction());
	target->Set(String::NewSymbol("channel_on_data"),
        FunctionTemplate::New(channel_on_data)->GetFunction());
	target->Set(String::NewSymbol("record_sync"),
        FunctionTemplate::New(record_sync)->GetFunction());
	target->Set(String::NewSymbol("channel_on_data_sync"),
        FunctionTemplate::New(channel_on_data_sync)->GetFunction());
    target->Set(String::NewSymbol("internal_status"),
        FunctionTemplate::New(internal_status)->GetFunction());
}

NODE_MODULE(drecorder, RegisterModule);
