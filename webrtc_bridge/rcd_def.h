/******************************************************************************
        (c) COPYRIGHT 2011-2012 by Shenzhen DinStar Technologies Co.,Ltd
                          All rights reserved.
File: rcd_def.h
Desc: the definition header file of the scp common management
Modification history(No., author, date, desc)
1 Nate 2012-05-03 create file

******************************************************************************/
#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif /* __cplusplus */
#endif /* __cplusplus */

#ifndef _RCD_DEF_H_
#define _RCD_DEF_H_

#define RCD_MAX_NUM_LEN		        31
#define RCD_MSG_RESENT_T            500     //500 毫秒
#define RCD_MAX_MSG_RESENT_TIME     3
#define RCD_LNK_DETECT_T            5000     //5秒
#define RCD_DEFAULT_UDP_PORT        2999

/*消息类型*/
typedef enum
{
	RECORD_START,		/*  录音开始*/
	RECORD_START_ACK,	/*  录音开始确认*/	
	RECORD_STOP,		/*  录音结束*/
	RECORD_STOP_ACK,	/*  录音结束确认*/
	LNK_DETECT,         /*  链路检测消息*/
	LNK_DETECT_ACK,     /*  链路检测消息确认*/

	RECORD_MSG_BUTT
}RECORD_MSG_TYPE_E;

/*ACK 错误码枚举*/
enum
{
    RECORD_SUCC                         = 0,            /* 成功*/
    RECORD_SERV_UNAVA                   = 1,            /* 服务不可用*/
    RECORD_NO_SERV_RIGHTS               = 2,            /* 无业务权限 */

    RECORD_CAUSE_BUTT
};

typedef enum
{
    RECORD_SRV_UP                       = 0,            /* 录音服务器可用*/
    RECORD_SRV_DOWN                     = 1,            /* 录音服务器不可用*/

    RECORD_SRV_BUTT
}RECORD_SRV_STATUS_TYPE_E;


typedef struct
{
    U8              ucMsgType;         /*  消息类型*/    
    U8              ucMsgSn;           /*  消息序号*/
    U16             usCallId;		   /*  呼叫标示, 短时间内不会重复*/

    S8              szCalling[RCD_MAX_NUM_LEN+1];
    S8              szCalled[RCD_MAX_NUM_LEN+1];

}RECORD_MSG_S;

typedef struct
{
    U8              ucMsgType;          /*  消息类型*/    
    U8              ucMsgSn;           /*  消息序号*/
    U16             usCallId;		   /*  呼叫标示, 短时间内不会重复*/

    U8              ucErrCode;		    /*  错误码, 用来标志响应结果*/
    U8              ucReserve1;         /*  保留*/
    U16             usReserve2;         /*  保留*/
    U16             usRcvTDMRxUdpPort;  /*  录音服务器接收TDM Rx 录音的端口*/
    U16             usRcvTDMTxUdpPort;  /*  录音服务器接收TDM Tx 录音的端口*/
    U32             ulRcvIP;            /*  录音服务器接收录音的IP */

}RECORD_MSG_ACK_S;

typedef struct
{
    U8              ucMsgType;         /*  消息类型*/    
    U8              ucMsgSn;           /*  消息序号*/
    
    U16             usCallId;		   /*  呼叫标示, 短时间内不会重复,  此处不关心, 置0*/
}LNK_DETECT_MSG_S;

typedef struct
{
    U8              ucMsgType;         /*  消息类型*/    
    U8              ucMsgSn;           /*  消息序号*/
    
    U16             usCallId;		   /*  呼叫标示, 短时间内不会重复,  此处不关心, 置0*/
}LNK_DETECT_MSG_ACK_S;


#endif/*_RCD_DEF_H_*/

#ifdef __cplusplus
#if __cplusplus
}
#endif /* __cplusplus */
#endif /* __cplusplus */



