/**
 * Created by Rainc on 15-1-4.
 */

var api = require('./controllers/api');

module.exports = function (app) {
  app.get('/',function(req,res){
    res.render('index.html')
  });

  app.get('/web',function(req,res){
    res.redirect('/')
  });

  app.get('/api',api.recordApi);
  app.post('/api',api.recordApi);

}