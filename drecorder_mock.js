var fs = require('fs');

var nconf = require('nconf');
var winston = require('winston');

var mtg_proto = require('./mtg_proto');

var G_RECORDERS = {};
var G_LOGGER;

const MAX_SEQ = 0xffff;
const MAX_TS = 0x100000000;
const TS_PER_BUFFER = 1000;
var G_8K_CNG;

function make_cng(fs_hz) {
  var buf = new Buffer(fs_hz*2);
  for(var i = 0; i < fs_hz; ++i) {
  }
}

///////////////////////////////////////////////////////////////////////////////
// DRChannel
// audio channel, put media frame to jitter buffer
// @params
// id:
///////////////////////////////////////////////////////////////////////////////
function DRChannel() {
  this.id = arguments[0];
  this.fs_hz = arguments[1];
  this.samples_per_sec = arguments[2];
  this.ptime = arguments[3];
  //this.packets_per_sec = 1000/this.ptime;
  this.samples_10ms = (this.samples_per_sec/100)*(this.ptime/10); // 10ms samples
  this.packet_size = 0;
  this.base_ts = -1;
  this.first_ts = -1;
  this.buffers = []; //buffer list. per buffer contain one second samples.
  this.packets_count = 0;
  this.offset = 0;
}

function wrapup(base, ts, spacing) {
  if((base + spacing) >= MAX_TS && ts < spacing) {
    return ts + MAX_TS;  // 0 base index
  } else {
    return ts;
  }
}

function ts2ms(ts) { //adapt ts as milli-secs or samples(webrtc)
  return ts;
}

DRChannel.prototype.get_buffer = function(ts) {
  var base = this.base_ts;
  ts = wrapup(base, ts, TS_PER_BUFFER, MAX_TS);
  if(ts <= base) {
    G_LOGGER.debug("%d channel received delay packet", this.id);
    return;
  }
  var ms = ts2ms(ts - base);
  var buf_idx = (ms/TS_PER_BUFFER)>>>0; //calc sec-buffer index
  var offset = ms%TS_PER_BUFFER;

  if(buf_idx == this.buffers.length) { // overflow then add new buffer 
    this.buffers.push(new Buffer(this.samples_per_sec));
  } else if(buf_idx > this.buffers.length) { // jitter range is 1s delay or 1s forward.
    G_LOGGER.error("seq too large: %d vs %d", seq, base);
    return;
  }
  return [offset, this.buffers[buf_idx]];
};

DRChannel.prototype.release_buffer = function() {
  var buf = this.buffers.shift();
  if(!buf) { // no available data
    buf = new Buffer(this.samples_per_sec);
  } 

  this.base_ts += TS_PER_BUFFER;
  //wrapup handle
  if(this.base_ts >= MAX_TS) {
    this.base_ts -= MAX_TS;
  }
  return buf;
}

DRChannel.prototype.on_data = function(data) {
  var frame = mtg_proto.parse_audio_packet(data); //get frame after parse raw data
  var payload = frame.payload;
  if(!this.packet_size) {
    this.packet_size = payload.length;
    this.first_ts = frame.timestamp;
    this.base_ts = frame.timestamp;
    G_LOGGER.info("received first packet");
  }
  var info = this.get_buffer(frame.timestamp);
  if(!info) {
    G_LOGGER.info("%d channel drop packet %d", this.id, frame.timestamp);
    return;
  }
  var buffer = info[1];
  var offset = info[0];
  buffer.copy(payload, offset);
}

DRChannel.prototype.get_10ms_data = function() {
  var buf = this.buffers[0];
  var end = this.offset + this.samples_10ms;
  var result = buf.slice(this.offset, end);
  this.offset = end;
  if(this.offset == buf.length) {
    this.release_buffer();
    this.offset = 0;
  }
  return result;
};

///////////////////////////////////////////////////////////////////////////////
//
// DRecorder
// @params
//  id: recorder id
//  caller:  
//  called: 
//  file: recorded audio file
//  format: 
///////////////////////////////////////////////////////////////////////////////
function DRecorder() {
  this.id = arguments[0];
  this.caller = arguments[1];
  this.called = arguments[2];
  this.file = arguments[3];
  this.format = arguments[4];
};


DRecorder.prototype.add_channels = function(count) {
  var chan_ids = [];
  this.channels  = [];
  for(var i = 0; i < count; ++i) {
    this.channels.push(new DRChannel(i));
    chan_ids.push(i);
  }
  return chan_ids;
};

DRecorder.prototype.get_channel = function(chan_id) {
  return this.channels[chan_id];
};

DRecorder.prototype.record = function() {
  var channel = this.channels[0];
  var data = channel.get_10ms_data();
  var data1 = this.channels[1].get_10ms_data();
  var result = _drecorder.mix_audio(data, data1);
  fs.write(this.file, result);
};


function init(log_path, log_level) {
  G_LOGGER = require('./logger_factory').get(nconf.get("logger_factory"));
}

function start_recorder(session_id, caller, called, options) {
  var out_dir = options.out_dir;
  fs.open_sync()
  var recorder = new DRecorder(session_id, req.caller, req.called, options);
  G_RECORDERS[session_id] = recorder;
}

function stop_recorder(session_id) {
}

function add_channels(session_id, count) {
  var recorder = G_RECORDERS[session_id];
  if(recorder) {
    return recorder.add_channels(count);
  } else {
    throw TypeError("no recorder "+session_id);
  }
}

function internal_status() {
}

function record(session_id, callback) {
  var recorder = G_RECORDERS[session_id];
  if(recorder) {
    return recorder.record(callback);
  } else {
    throw TypeError("no recorder "+session_id);
  }
}

function record_sync(session_id) {
}

function channel_on_data_sync(session_id, chan_id, data) {
}

function channel_on_data(session_id, chan_id, data, callback) {
var recorder = G_RECORDERS[session_id];
  if(recorder) {
    var channel = recorder.get_channel(chan_id);
    return channel.on_data(data, callback);
  } else {
    throw TypeError("no recorder "+session_id);
  }
}

