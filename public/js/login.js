/**
 * Created by Rainc on 15-1-6.
 */

var lan = window.location.search.split('?')[1];

$(document).ready(function() {

  $("#userName").kendoMaskedTextBox({
  });

  $("#password").kendoMaskedTextBox({
  });

  $("#login").kendoButton({
    click: onLoginClick
  });

  $("#language").kendoMenu({
    select: onLanguageSelect
  });
  document.onkeydown=function(){
    if (event.keyCode == 13){
      onLoginClick();
    }
  }
});

function onLoginClick(e){
  if($('#userName').val() == 'admin' && $('#password').val() == 'admin'){
    $.cookie('userName',$('#userName').val());
    $.cookie('password',$('#password').val());
    location.href = "index.html?" + lan;
  }else{
    window.alert("登录失败！");
  }
}

function onLanguageSelect(e){
//  console.log($(e.item).children(".k-link").text());
  if($(e.item).children(".k-link").text() == 'English' || $(e.item).children(".k-link").text() == '英文'){
    location.href = "loginEN.html?EN"
  }else if($(e.item).children(".k-link").text() == 'Chinese' || $(e.item).children(".k-link").text() == '中文'){
    location.href = "loginCN.html?CN"
  }
}