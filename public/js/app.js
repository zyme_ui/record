/**
 * Created by Rainc on 15-1-5.
 */

$(document).ready(function() {
  //初始化语言
  window.lang = new Lang('en','en',true);
  lan = window.location.search.split('?')[1] || '';

  //设置内容自适应屏幕高度
  $("#content").height($(window).height() * 0.85);

  //at the beginning,hide the statusFrom
  $('#reServerInfo').hide();
  $('#reGrid1').hide();
  $('#reGrid2').hide();
  $('#reGrid3').hide();
  $('#reStatus').show();

  //kendo中文化
  kendo.culture("zh-CN");

  //定时刷新状态信息
  setInterval(function(){
    $.ajax({url:'/api?action=getStatus',success: function(data,status){
      $("#system_uptime")[0].value = data.code.runtime.system_uptime;
      $("#process_uptime")[0].value = data.code.runtime.process_uptime;
      $("#systemMemory")[0].value = data.code.runtime.total_memory + ' MB';
      $("#memoryUtilization")[0].value = data.code.runtime.memoryUtilization  + ' %';
      $("#loadavg_1m")[0].value = data.code.runtime.loadavg_1m + ' %';
      $("#rx_rate")[0].value = data.code.runtime.rx_rate + ' KBPS';
      $("#tx_rate")[0].value = data.code.runtime.tx_rate + ' KBPS';
      $("#diskUtilization")[0].value = data.code.runtime.diskUtilization  + ' %';
      $("#disk_used")[0].value = data.code.runtime.disk_used + ' MB';

      $("#listen_ip")[0].value = data.code.app_config.listen_ip;
      $("#listen_port")[0].value = data.code.app_config.listen_port;
      $("#server_version")[0].value = data.code.product_info.version;
      $("#max_sessions")[0].value = data.code.app_config.max_sessions;
      $("#sessions_count")[0].value = data.code.app_status.sessions_count;
      $("#processed_total")[0].value = data.code.app_status.processed_total;
      $("#processed_failed")[0].value = data.code.app_status.processed_failed;
      $("#server_id")[0].value = data.code.product_info.server_id;
//      $("#product_name")[0].value = data.code.product_info.product_name;
//      $("#product_id")[0].value = data.code.product_info.product_id;

      $("#audio_time")[0].value = data.code.engine_stats.audio_time;
      $("#mix_time")[0].value = data.code.engine_stats.mix_time;
      $("#write_time")[0].value = data.code.engine_stats.write_time;
      $("#record_time")[0].value = data.code.engine_stats.record_time;
      $("#fraction_lost")[0].value = data.code.engine_stats.fraction_lost;
      $("#cumulative_lost")[0].value = data.code.engine_stats.cumulative_lost;
      $("#extended_max_sequence_number")[0].value = data.code.engine_stats.extended_max_sequence_number;
      $("#jitter")[0].value = data.code.engine_stats.jitter;
      $("#current_buffer_size_ms")[0].value = data.code.engine_stats.current_buffer_size_ms;
      $("#preferred_buffer_size_ms")[0].value = data.code.engine_stats.preferred_buffer_size_ms;
      $("#jitter_peaks_found")[0].value = data.code.engine_stats.jitter_peaks_found;
      $("#packet_loss_rate")[0].value = data.code.engine_stats.packet_loss_rate;
      $("#packet_discard_rate")[0].value = data.code.engine_stats.packet_discard_rate;
      $("#expand_rate")[0].value = data.code.engine_stats.expand_rate;
      $("#preemptive_rate")[0].value = data.code.engine_stats.preemptive_rate;
      $("#accelerate_rate")[0].value = data.code.engine_stats.accelerate_rate;
      $("#clockdrift_ppm")[0].value = data.code.engine_stats.clockdrift_ppm;
      $("#added_zero_samples")[0].value = data.code.engine_stats.added_zero_samples;

//      $("#server")[0].innerHTML = data.code.product_info.product_name + " | " + data.code.product_info.product_id;
      $("#timeStamp")[0].innerHTML = data.code.timestamp;
    }})
  },5000)

  //if no login info ,redirect to loginEN.html
  if($.cookie('userName') == undefined || $.cookie('userName') == null){
    location.href="loginCN.html?CN";
  }

  //基本
  $("#system_uptime").kendoAutoComplete({});
  $("#process_uptime").kendoAutoComplete({});
  $("#systemMemory").kendoAutoComplete({});
  $("#memoryUtilization").kendoAutoComplete({});
  $("#loadavg_1m").kendoAutoComplete({});
  $("#rx_rate").kendoAutoComplete({});
  $("#tx_rate").kendoAutoComplete({});
  $("#diskUtilization").kendoAutoComplete({});
  $("#disk_used").kendoAutoComplete({});
  $("#serverIp").kendoAutoComplete({});
  $("#serverPort").kendoAutoComplete({});
  $("#serverUuid").kendoAutoComplete({});
  $("#productId").kendoAutoComplete({});
  $("#listen_ip").kendoAutoComplete({});
  $("#listen_port").kendoAutoComplete({});
  $("#server_version").kendoAutoComplete({});
  $("#max_sessions").kendoAutoComplete({});
  $("#sessions_count").kendoAutoComplete({});
  $("#processed_total").kendoAutoComplete({});
  $("#processed_failed").kendoAutoComplete({});
  $("#server_id").kendoAutoComplete({});
//  $("#product_name").kendoAutoComplete({});
//  $("#product_id").kendoAutoComplete({});
  //高级
  $("#audio_time").kendoAutoComplete({});
  $("#mix_time").kendoAutoComplete({});
  $("#write_time").kendoAutoComplete({});
  $("#record_time").kendoAutoComplete({});
  $("#fraction_lost").kendoAutoComplete({});
  $("#cumulative_lost").kendoAutoComplete({});
  $("#extended_max_sequence_number").kendoAutoComplete({});
  $("#jitter").kendoAutoComplete({});
  $("#current_buffer_size_ms").kendoAutoComplete({});
  $("#preferred_buffer_size_ms").kendoAutoComplete({});
  $("#jitter_peaks_found").kendoAutoComplete({});
  $("#packet_loss_rate").kendoAutoComplete({});
  $("#packet_discard_rate").kendoAutoComplete({});
  $("#expand_rate").kendoAutoComplete({});
  $("#preemptive_rate").kendoAutoComplete({});
  $("#accelerate_rate").kendoAutoComplete({});
  $("#clockdrift_ppm").kendoAutoComplete({});
  $("#added_zero_samples").kendoAutoComplete({});

  $("#logout").kendoButton();
  $("#setting").kendoButton();
  $("#saveSetting").kendoButton();

//  $("#toolbar").kendoToolBar({
//    resizable: true,
//    height: '50px',
//    items: [
//      {
//        template: "<div id='logo' style='font-size: 2.5em;margin-left: 20px'>DINSTAR</div>",
//        overflow: "never"
//      },
//      {
//        template: "<span >&nbsp;</span>"
//      },
//      {
//        type: "splitButton",
//        text: "Language",
//        id: "Language",
//        menuButtons: [
//          { id: "lanCN", text: "CN" , group: "lan"},
//          { id: "lanEN", text: "EN" , group: "lan"}
//        ]
//      },
//      {
//        template: "<span id='currentLan'>" + lan + "</span>"
//      },
//      {
//        type: "button",
//        text: "Logout",
//        id: "logout",
//        overflow: "always"
//      }
//    ],
//    click: toolBarClick
//  });

  //判断初始语言
  if( lan == 'CN'){
    window.lang.dynamic('cn', 'json/langpack/cn.json');
    window.lang.change('cn');
    $("#logout")[0].innerHTML = "登出";
    $("#dropdown").kendoDropDownList({
      dataTextField: "text",
      dataValueField: "value",
      select:onDropDownSelect,
      dataSource: [{ text: "中文", value: "1" },{ text: "EN", value: "2" }],
      index: 0 // 当前默认选中项，索引从0开始。
    });
    $("#showInfo").kendoDropDownList({
      dataTextField: "text",
      dataValueField: "value",
      select:onShowInfoSelect,
      dataSource: [{ text: "基 本", value: "1" },{ text: "高 级", value: "2" }],
      index: 0 // 当前默认选中项，索引从0开始。
    });
    setTimeout(function(){
      $("#dropdown").data("kendoDropDownList").select(0)
      $("#showInfo").data("kendoDropDownList").select(0);
    },200)
    $("#panelbar").kendoPanelBar({
      expandMode: "single",
      select: onPanelBarSelect,
      dataSource:[
        {
          text: "服务器",
          expanded: true,
          items: [
            { text: "录音服务信息"},
            { text: "录音文件列表"},
            { text: "录音备份文件列表"}
          ]
        }
      ]
    });
    $('#advancedInfo').hide();
    $('#baseInfo').show();
  }else{
    window.lang.change('en');
    $("#panelbar").kendoPanelBar({
      expandMode: "single",
      select: onPanelBarSelect,
      dataSource:[
        {
          text: "SERVER",
          expanded: true,
          items: [
            { text: "RECORD SERVICE INFO"},
            { text: "RECORD FILE LIST"},
            { text: "RECORD PACK LIST"}
          ]
        }
      ]
    });
    $("#dropdown").kendoDropDownList({
      dataTextField: "text",
      dataValueField: "value",
      select:onDropDownSelect,
      dataSource: [{ text: "中文", value: "1" },{ text: "EN", value: "2" }],
      index: 0 // 当前默认选中项，索引从0开始。
    });
    $("#showInfo").kendoDropDownList({
      dataTextField: "text",
      dataValueField: "value",
      select:onShowInfoSelect,
      dataSource: [{ text: "BaseInfo", value: "1" },{ text: "Advanced", value: "2" }],
      index: 0 // 当前默认选中项，索引从0开始。
    });
    setTimeout(function(){
      $("#dropdown").data("kendoDropDownList").select(1)
      $("#showInfo").data("kendoDropDownList").select(0);
    },200)
    $('#advancedInfo').hide();
    $('#baseInfo').show();
  }

  var Media = document.getElementById('media');
  Media.addEventListener('play',function(){
  });
  Media.addEventListener('pause',function(){
  });
  Media.addEventListener('abort',function(){
  });
  Media.addEventListener('ended',function(){
    $('.icon-pause').removeClass('icon-pause').addClass('icon-play');
  });

});

function onReGridChange(arg){
  $("#reGrid1").hide();
  $("#reGrid3").hide();

  var fileName = '';
  $.map(this.select(), function(item) {
    fileName =  item.childNodes[0].textContent;
  });
  var dataSource = new kendo.data.DataSource({
    type: "json",
    transport: {
      read: "/api?action=getRecordList&dir=" + fileName
    },
    schema: {
      model: {
        id:"id",
        fields: {
          fileCtime: { type: "string" },
          caller: { type: "string" },
          called: { type: "string" },
          fileSize: { type: "string" },
          MTG_ADDRESS:{type: "string"},
          fileName: { type: "string" },
          filePath: { type: "string" },
          parentFileName: { type: "string" }
        }
      },
      parse: function (data) {
        $.each(data, function (index, item) {
          item.id = index;
        });
        return data;
      }
    },
    pageSize: 10
  });
//  if($('#reGrid2').data('kendoGrid') && Catalogue == fileName){
//    $("#reGrid2").show();
//  }else{
//    Catalogue = fileName;          //录音日期目录
    if(lan == 'CN'){
      $("#reGrid2").kendoGrid({
        dataSource:dataSource,
        data:function (response) {
          return response.data;
        },
        height: 'auto',
        filterable: true,
        selectable:"row",
        change: showFileName,
        resizable: true,
        sortable: true,
        toolbar: kendo.template($("#goBack").html()),
        pageable: {
          refresh: true,
          pageSizes: true,
          buttonCount: 5
        },
        columns: [
          {
            field:"fileCtime",
            width: 175,
            title: "创建时间"
          },
          {
            field:"caller",
            title: "主叫号码",
            width: 115
          },
          {
            field:"called",
            title: "被叫号码",
            width: 115
          },
          {
            field:"fileSize",
            width: 115,
            title: "大小(KB)"
          },
          {
            field:"MTG_ADDRESS",
            title: "音源地址",
            width: 150
          },
          {
            field:"fileName",
            title: "名称",
            width: 140
          },
        {
          field:"filePath",
          title: "文件路径",
          hidden:true
        },
        {
          field:"parentFileName",
          title: "上层目录名称",
          hidden:true
        },
          {
            field:"action",
            width: 100,
            title:"动作",
            filterable:false,
            locked: true,
            lockable: false,
            template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#','#=parentFileName#','#= id#')"+"></span></div>"
          }
        ]
      }).show();
      $("#goBackButton").kendoButton();
//      setTimeout(function(){
//        $("#reGrid2").show();
//      },500)
    }else{
      $("#reGrid2").kendoGrid({
        dataSource: dataSource,
        data:function (response) {
          return response.data;
        },
        height: 'auto',
        filterable: true,
        selectable:"row",
        change: showFileName,
        resizable: true,
        sortable: true,
        toolbar: kendo.template($("#goBack").html()),
        pageable: {
          refresh: true,
          pageSizes: true,
          buttonCount: 5
        },
        columns: [
          {
            field:"fileCtime",
            width: 175,
            title: "fileCtime"
          },
          {
            field:"caller",
            title: "caller",
            width: 115
          },
          {
            field:"called",
            title: "called",
            width: 115
          },
          {
            field:"fileSize",
            width: 115,
            title: "fileSize(KB)"
          },
          {
            field:"MTG_ADDRESS",
            title: "MTG_ADDRESS",
            width: 150
          },
          {
            field:"fileName",
            title: "fileName",
            width: 140
          },
          {
            field:"filePath",
            title: "filePath",
            hidden:true
          },
          {
            field:"parentFileName",
            title: "parentFileName",
            hidden:true
          },
          {
            field:"action",
            width: 100,
            title:"action",
            filterable:false,
            locked: true,
            lockable: false,
            template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large'  onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#','#=parentFileName#')"+"></span></div>"
          }
        ]
      }).show();
      $("#goBackButton").kendoButton();
    }
//  }
}

function goBack(){
  $("#reGrid2").empty().hide();
//  $("#reGrid2").hide();
//  $("#reGrid2").data("kendoGrid").dataSource.data([])
  $("#reGrid3").hide();
  if($("#reGrid1").data('kendoGrid')){
    $("#reGrid1").show();
  }else{
    if(lan == 'CN'){
      $("#reGrid1").kendoGrid({
        dataSource:{
          type: "json",
          transport: {
            read: "/api?action=getRecordList"
          },
          schema: {
            model: {
              fields: {
                fileName: { type: "string" },
                filePath: { type: "string" }
              }
            }
          },
          pageSize: 10
        },
        data:function (response) {
          return response.data;
        },
        height: 'auto',
        groupable: false,
        filterable: true,
        selectable:"row",
        resizable: true,
        sortable: true,
        toolbar: kendo.template($("#reGridToolbar").html()),
        change: onReGridChange,
        pageable: {
          refresh: true,
          pageSizes: true,
          buttonCount: 5
        },
        columns: [
          {
            field:"fileName",
            title: "文件名称"
          },
          {
            field:"filePath",
            title: "文件路径",
            hidden:true
          }
        ]
      });
      setTimeout(function(){
        $("#reGrid1").show();
      },100)
    }else{
      $("#reGrid1").kendoGrid({
        dataSource:{
          type: "json",
          transport: {
            read: "/api?action=getRecordList"
          },
          schema: {
            model: {
              fields: {
                fileName: { type: "string" },
                filePath: { type: "string" }
              }
            }
          },
          pageSize: 10
        },
        data:function (response) {
          return response.data;
        },
        height: 'auto',
        groupable: false,
        filterable: true,
        selectable:"row",
        resizable: true,
        sortable: true,
        toolbar: kendo.template($("#reGridToolbar").html()),
        change: onReGridChange,
        pageable: {
          refresh: true,
          pageSizes: true,
          buttonCount: 5
        },
        columns: [
          {
            field:"fileName",
            title: "fileName"
          },
          {
            field:"filePath",
            title: "filePath",
            hidden:true
          }
        ]
      });
      setTimeout(function(){
        $("#reGrid1").show();
      },100)
    }
  }
}

function audition(filePath,fileName,a){
  var src = '';
  if(filePath == ''){
    src = fileName;
  }else{
    src = filePath + '/' + fileName;
  }
  document.getElementById('mediaControl').style.display = 'block';
  var Media = document.getElementById('media');
  Media.src = src;

  if($(a).hasClass('icon-play')){
    Media.play();
    $('.icon-pause').removeClass('icon-pause').addClass('icon-play');
    $(a).removeClass('icon-play').addClass('icon-pause')
  }else{
    Media.pause();
    $(a).removeClass('icon-pause').addClass('icon-play');
  }
}

function download(filePath,fileName){
  var filePath = encodeURIComponent(filePath);
  location.href = '/api?action=download&filePath=' + filePath + "&fileName=" + fileName;
}

function deleteFile(filePath,fileName,parentFileName,id){
  var filePath = encodeURIComponent(filePath);
  $.ajax({url: '/api?action=deleteFile&filePath='+ filePath + '&fileName=' + fileName,success: function(data,status){
    if(data.success == true){
      window.alert('删除成功！');
      if(!parentFileName){
        if(lan == 'CN'){
          $("#reGrid3").data("kendoGrid").dataSource.remove($("#reGrid3").data("kendoGrid").dataSource.get(id));
//          $("#reGrid3").kendoGrid({
//            dataSource:{
//              type: "json",
//              transport: {
//                read: "/api?action=getRecordPack"
//              },
//              schema: {
//                model: {
//                  fields: {
//                    fileCtime: { type: "string" },
//                    caller: { type: "string" },
//                    called: { type: "string" },
//                    fileSize: { type: "string" },
//                    MTG_ADDRESS:{type: "string"},
//                    fileName: { type: "string" },
//                    filePath: { type: "string" }
//                  }
//                }
//              },
//              pageSize: 10
//            },
//            data:function (response) {
//              return response.data;
//            },
//            height: 'auto',
//            filterable: true,
//            selectable:"row",
//            sortable: true,
//            resizable: true,
//            toolbar: kendo.template($("#reGridToolbar").html()),
//            pageable: {
//              refresh: true,
//              pageSizes: true,
//              buttonCount: 5
//            },
//            columns: [
//              {
//                field:"fileCtime",
//                width: 175,
//                title: "创建时间"
//              },
//              {
//                field:"caller",
//                title: "主叫号码",
//                width: 115
//              },
//              {
//                field:"called",
//                title: "被叫号码",
//                width: 115
//              },
//              {
//                field:"fileSize",
//                width: 115,
//                title: "大小(KB)"
//              },
//              {
//                field:"MTG_ADDRESS",
//                title: "音源地址",
//                width: 150
//              },
//              {
//                field:"fileName",
//                title: "名称",
//                width: 140
//              },
//              {
//                field:"filePath",
//                title: "文件路径",
//                hidden:true
//              },
//              {
//                field:"action",
//                width: 100,
//                title:"动作",
//                filterable:false,
//                locked: true,
//                lockable: false,
//                template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large'  src='img/download.png' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#')"+"></span></div>"
//              }
//            ]
//          })
        }else{
          $("#reGrid3").data("kendoGrid").dataSource.remove($("#reGrid3").data("kendoGrid").dataSource.get(id));
//          $("#reGrid3").kendoGrid({
//            dataSource:{
//              type: "json",
//              transport: {
//                read: "/api?action=getRecordPack"
//              },
//              schema: {
//                model: {
//                  fields: {
//                    fileCtime: { type: "string" },
//                    caller: { type: "string" },
//                    called: { type: "string" },
//                    fileSize: { type: "string" },
//                    MTG_ADDRESS:{type: "string"},
//                    fileName: { type: "string" },
//                    filePath: { type: "string" }
//                  }
//                }
//              },
//              pageSize: 10
//            },
//            data:function (response) {
//              return response.data;
//            },
//            height: 'auto',
//            groupable: false,
//            filterable: true,
//            selectable:"row",
//            sortable: true,
//            resizable: true,
//            toolbar: kendo.template($("#reGridToolbar").html()),
//            pageable: {
//              refresh: true,
//              pageSizes: true,
//              buttonCount: 5
//            },
//            columns: [
//              {
//                field:"fileCtime",
//                width: 175,
//                title: "fileCtime"
//              },
//              {
//                field:"caller",
//                title: "caller",
//                width: 115
//              },
//              {
//                field:"called",
//                title: "called",
//                width: 115
//              },
//              {
//                field:"fileSize",
//                width: 115,
//                title: "fileSize(KB)"
//              },
//              {
//                field:"MTG_ADDRESS",
//                title: "MTG_ADDRESS",
//                width: 150
//              },
//              {
//                field:"fileName",
//                title: "fileName",
//                width: 140
//              },
//              {
//                field:"filePath",
//                title: "filePath",
//                hidden:true
//              },
//              {
//                field:"action",
//                width: 100,
//                title:"action",
//                filterable:false,
//                locked: true,
//                lockable: false,
//                template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large'  src='img/download.png' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#')"+"></span></div>"
//              }
//            ]
//          })
        }
      }else{
        if(lan == 'CN'){
          $("#reGrid2").data("kendoGrid").dataSource.remove($("#reGrid2").data("kendoGrid").dataSource.get(id));
//          setTimeout(function(){
//            $("#reGrid2").kendoGrid({
//              dataSource:{
//                type: "json",
//                transport: {
//                  read: "/api?action=getRecordList&dir=" + parentFileName
//                },
//                schema: {
//                  model: {
//                    fileName:"fileName"
//                  }
//                },
//                pageSize: 10
//              },
//              data:function (response) {
//                return response.data;
//              },
//              height: 'auto',
//              filterable: true,
//              selectable:"row",
//              change: showFileName,
//              resizable: true,
////              sortable: true,
//              toolbar: kendo.template($("#goBack").html()),
//              pageable: {
//                refresh: true,
//                pageSizes: true,
//                buttonCount: 5
//              },
//              columns: [
//                {
//                  field:"action",
//                  width: 100,
//                  title:"动作",
//                  filterable:false,
//                  locked: true,
//                  lockable: false,
//                  template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#','#=parentFileName#')"+"></span></div>"
//                },
//                {
//                  field:"fileCtime",
//                  width: 175,
//                  title: "创建时间"
//                },
//                {
//                  field:"caller",
//                  title: "主叫号码",
//                  width: 115
//                },
//                {
//                  field:"called",
//                  title: "被叫号码",
//                  width: 115
//                },
//                {
//                  field:"fileSize",
//                  width: 115,
//                  title: "大小(KB)"
//                },
//                {
//                  field:"MTG_ADDRESS",
//                  title: "音源地址",
//                  width: 150
//                },
//                {
//                  field:"fileName",
//                  title: "名称",
//                  width: 140
//                },
//                {
//                  field:"filePath",
//                  title: "文件路径",
//                  hidden:true
//                },
//                {
//                  field:"parentFileName",
//                  title: "上层目录名称",
//                  hidden:true
//                }
//              ]
//            })
//          },500)
        }else{
          $("#reGrid2").data("kendoGrid").dataSource.remove($("#reGrid2").data("kendoGrid").dataSource.get(id));
//          $("#reGrid2").kendoGrid({
//            dataSource:{
//              type: "json",
//              transport: {
//                read: "/api?action=getRecordList&dir=" + parentFileName
//              },
//              schema: {
//                model: {
//                  fields: {
//                    fileCtime: { type: "string" },
//                    caller: { type: "string" },
//                    called: { type: "string" },
//                    fileSize: { type: "string" },
//                    MTG_ADDRESS:{type: "string"},
//                    fileName: { type: "string" },
//                    filePath: { type: "string" },
//                    parentFileName: { type: "string" }
//                  }
//                }
//              },
//              pageSize: 10
//            },
//            data:function (response) {
//              return response.data;
//            },
//            height: 'auto',
//            filterable: true,
//            selectable:"row",
//            change: showFileName,
//            resizable: true,
//            sortable: true,
//            toolbar: kendo.template($("#goBack").html()),
//            pageable: {
//              refresh: true,
//              pageSizes: true,
//              buttonCount: 5
//            },
//            columns: [
//              {
//                field:"fileCtime",
//                width: 175,
//                title: "fileCtime"
//              },
//              {
//                field:"caller",
//                title: "caller",
//                width: 115
//              },
//              {
//                field:"called",
//                title: "called",
//                width: 115
//              },
//              {
//                field:"fileSize",
//                width: 115,
//                title: "fileSize(KB)"
//              },
//              {
//                field:"MTG_ADDRESS",
//                title: "MTG_ADDRESS",
//                width: 150
//              },
//              {
//                field:"fileName",
//                title: "fileName",
//                width: 140
//              },
//              {
//                field:"filePath",
//                title: "filePath",
//                hidden:true
//              },
//              {
//                field:"parentFileName",
//                title: "parentFileName",
//                hidden:true
//              },
//              {
//                field:"action",
//                width: 100,
//                title:"action",
//                filterable:false,
//                locked: true,
//                lockable: false,
//                template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large'  src='img/download.png' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#','#=parentFileName#')"+"></span></div>"
//              }
//            ]
//          })
        }
      }
    }
  }})
}

function onPanelBarSelect(e){
  console.log($(e.item).find("> .k-link").text());
  if($(e.item).find("> .k-link").text() == '录音服务信息'|| $(e.item).find("> .k-link").text() == 'RECORD SERVICE INFO'){
    var Media = document.getElementById('media');
    document.getElementById('mediaControl').style.display = 'none';
    Media.pause();
    $('#reStatus').show();
    $('#reGrid1').hide();
    $('#reGrid2').empty().hide();
    $('#reGrid3').hide();
  }else if($(e.item).find("> .k-link").text() == '录音文件列表'|| $(e.item).find("> .k-link").text() == 'RECORD FILE LIST'){
    $("#reGrid2").empty().hide();
    $('#reStatus').hide();
    $('#reGrid3').hide();
      if($("#reGrid1").data('kendoGrid')){
        $("#reGrid1").show();
      }else{
        if(lan == 'CN'){
          $("#reGrid1").kendoGrid({
            dataSource:{
              type: "json",
              transport: {
                read: "/api?action=getRecordList"
              },
              schema: {
                model: {
                  fields: {
                    fileName: { type: "string" },
                    filePath: { type: "string" }
                  }
                }
              },
              pageSize: 10
            },
            data:function (response) {
              return response.data;
            },
            height: 'auto',
            groupable: false,
            filterable: true,
            selectable:"row",
            resizable: true,
            sortable: true,
            toolbar: kendo.template($("#reGridToolbar").html()),
            change: onReGridChange,
            pageable: {
              refresh: true,
              pageSizes: true,
              buttonCount: 5
            },
            columns: [
              {
                field:"fileName",
                title: "文件名称"
              },
              {
                field:"filePath",
                title: "文件路径",
                hidden:true
              }
            ]
          });
          setTimeout(function(){
            $("#reGrid1").show();
          },100)
        }else{
        $("#reGrid1").kendoGrid({
          dataSource:{
            type: "json",
            transport: {
              read: "/api?action=getRecordList"
            },
            schema: {
              model: {
                fields: {
                  fileName: { type: "string" },
                  filePath: { type: "string" }
                }
              }
            },
            pageSize: 10
          },
          data:function (response) {
            return response.data;
          },
          height: 'auto',
          groupable: false,
          filterable: true,
          selectable:"row",
          resizable: true,
          sortable: true,
          toolbar: kendo.template($("#reGridToolbar").html()),
          change: onReGridChange,
          pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
          },
          columns: [
            {
              field:"fileName",
              title: "fileName"
            },
            {
              field:"filePath",
              title: "filePath",
              hidden:true
            }
          ]
        });
        setTimeout(function(){
          $("#reGrid1").show();
        },100)
      }
      }
  }else if($(e.item).find("> .k-link").text() == '录音备份文件列表'|| $(e.item).find("> .k-link").text() == 'RECORD PACK LIST'){
    $('#reStatus').hide();
    $('#reGrid2').empty().hide();
    $('#reGrid1').hide();
    if($("#reGrid3").data("kendoGrid")){
      $("#reGrid3").show();
    }else{
      if(lan == 'CN'){
        $("#reGrid3").kendoGrid({
          dataSource:{
            type: "json",
            transport: {
              read: "/api?action=getRecordPack"
            },
            schema: {
              model: {
                fields: {
                  fileCtime: { type: "string" },
                  caller: { type: "string" },
                  called: { type: "string" },
                  fileSize: { type: "string" },
                  MTG_ADDRESS:{type: "string"},
                  fileName: { type: "string" },
                  filePath: { type: "string" }
                }
              }
            },
            pageSize: 10
          },
          data:function (response) {
            return response.data;
          },
          height: 'auto',
          groupable: false,
          filterable: true,
          selectable:"row",
          resizable: true,
          sortable: true,
          toolbar: kendo.template($("#reGridToolbar").html()),
          pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
          },
          columns: [
            {
              field:"fileCtime",
              width: 175,
              title: "创建时间"
            },
            {
              field:"caller",
              title: "主叫号码",
              width: 115
            },
            {
              field:"called",
              title: "被叫号码",
              width: 115
            },
            {
              field:"fileSize",
              width: 115,
              title: "大小(KB)"
            },
            {
              field:"MTG_ADDRESS",
              title: "音源地址",
              width: 150
            },
            {
              field:"fileName",
              title: "名称",
              width: 140
            },
            {
              field:"filePath",
              title: "文件路径",
              hidden:true
            },
            {
              field:"action",
              width: 100,
              title:"动作",
              filterable:false,
              locked: true,
              lockable: false,
              template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large'  src='img/download.png' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#')"+"></span></div>"
            }
          ]
        }).show();
      }else{
        $("#reGrid3").kendoGrid({
          dataSource:{
            type: "json",
            transport: {
              read: "/api?action=getRecordPack"
            },
            schema: {
              model: {
                fields: {
                  fileCtime: { type: "string" },
                  caller: { type: "string" },
                  called: { type: "string" },
                  fileSize: { type: "string" },
                  MTG_ADDRESS:{type: "string"},
                  fileName: { type: "string" },
                  filePath: { type: "string" }
                }
              }
            },
            pageSize: 10
          },
          data:function (response) {
            return response.data;
          },
          height: 'auto',
          groupable: false,
          filterable: true,
          selectable:"row",
          resizable: true,
          sortable: true,
          toolbar: kendo.template($("#reGridToolbar").html()),
          pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
          },
          columns: [
            {
              field:"fileCtime",
              width: 175,
              title: "fileCtime"
            },
            {
              field:"caller",
              title: "caller",
              width: 115
            },
            {
              field:"called",
              title: "called",
              width: 115
            },
            {
              field:"fileSize",
              width: 115,
              title: "fileSize(KB)"
            },
            {
              field:"MTG_ADDRESS",
              title: "MTG_ADDRESS",
              width: 150
            },
            {
              field:"fileName",
              title: "fileName",
              width: 140
            },
            {
              field:"filePath",
              title: "filePath",
              hidden:true
            },
            {
              field:"action",
              width: 100,
              title:"action",
              filterable:false,
              locked: true,
              lockable: false,
              template:"<div style='cursor:pointer'><span class='icon-play icon-large' onclick="+"audition('#=filePath#','#=fileName#',this)"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-download icon-large'  src='img/download.png' onclick="+"download('#=filePath#','#=fileName#')"+">&nbsp;&nbsp;&nbsp;</span><span class='icon-trash icon-large' onclick="+"deleteFile('#=filePath#','#=fileName#')"+"></span></div>"
            }
          ]
        }).show();
      }
    }
  }
}

function showFileName(){
  var fileName = '';
  $.map(this.select(), function(item) {
    if($(item)[0].childNodes[5]){
      fileName = $(item)[0].childNodes[5].textContent;
    }
  });
  $("#showFileName")[0].innerHTML = fileName;
}

function onDropDownSelect(e){
  var dataItem = this.dataItem(e.item.index());
  if(dataItem.text == 'CN' || dataItem.text == '中文'){
//    kendo.culture("zh-CN");
//    window.lang.dynamic('cn', 'json/langpack/cn.json');
//    window.lang.change('cn');
//    $("#logout")[0].innerHTML = "登出";
//    $("#dropdown").kendoDropDownList({
//      dataTextField: "text",
//      dataValueField: "value",
//      select:onDropDownSelect,
//      dataSource: [{ text: "CN", value: "1" },{ text: "英文", value: "2" }],
//      index: 0 // 当前默认选中项，索引从0开始。
//    });
//    setTimeout(function(){
//      $("#dropdown").data("kendoDropDownList").select(0)
//    },200)
    location.href = "index.html?CN"
  }else{
//    kendo.culture("en-US");
//    window.lang.change('en');
//    $("#logout")[0].innerHTML = "Logout";
//    $("#dropdown").kendoDropDownList({
//      dataTextField: "text",
//      dataValueField: "value",
//      select:onDropDownSelect,
//      dataSource: [{ text: "CN", value: "1" },{ text: "英文", value: "2" }],
//      index: 0 // 当前默认选中项，索引从0开始。
//    });
//    setTimeout(function(){
//      $("#dropdown").data("kendoDropDownList").select(1)
//    },200)
    location.href = "index.html?EN"
  }
}

function onShowInfoSelect(e){
  var dataItem = this.dataItem(e.item.index());
  if(dataItem.text == '基 本' || dataItem.text == 'BaseInfo'){
    $('#advancedInfo').hide();
    $('#baseInfo').show();
  }else {
    $('#baseInfo').hide();
    $('#advancedInfo').show();
  }
}

function logOutClick(){
  $.cookie('userName',null);
  location.href="loginCN.html?CN";
}

function enableSetting(){
//  document.getElementById('listen_ip').disabled = "";
//  document.getElementById('listen_port').disabled = "";
  document.getElementById('server_version').disabled = "";
  document.getElementById('server_id').disabled = "";
//  document.getElementById('product_name').disabled = "";
//  document.getElementById('product_id').disabled = "";
//  document.getElementById('listen_ip').style.backgroundColor = "";
//  document.getElementById('listen_port').style.backgroundColor = "";
  document.getElementById('server_version').style.backgroundColor = "";
  document.getElementById('server_id').style.backgroundColor = "";
//  document.getElementById('product_name').style.backgroundColor = "";
//  document.getElementById('product_id').style.backgroundColor = "";
  $("#saveSetting").show();
  $("#setting").hide();
}

function saveSetting(){
  var data = {
    server_version: document.getElementById('server_version').value,
    server_id: document.getElementById('server_id').value
//    product_name: document.getElementById('product_name').value,
//    product_id: document.getElementById('product_id').value
  };
  $.post('/api?action=saveSetting',data,function(result){
    console.log(result);
    if(result.success == true){
      $("#setting").show();
      $("#saveSetting").hide();

//      document.getElementById('listen_ip').disabled = "disabled";
//      document.getElementById('listen_port').disabled = "disabled";
      document.getElementById('server_version').disabled = "disabled";
      document.getElementById('server_id').disabled = "disabled";
//      document.getElementById('product_name').disabled = "disabled";
//      document.getElementById('product_id').disabled = "disabled";
//      document.getElementById('listen_ip').style.backgroundColor = "lightgrey";
//      document.getElementById('listen_port').style.backgroundColor = "lightgrey";
      document.getElementById('server_version').style.backgroundColor = "lightgrey";
      document.getElementById('server_id').style.backgroundColor = "lightgrey";
//      document.getElementById('product_name').style.backgroundColor = "lightgrey";
//      document.getElementById('product_id').style.backgroundColor = "lightgrey";
      alert('设置成功！');
    }else{
      alert('设置失败，err:' + result.code );
    }
  });
}

function enableAdvanced(){

}

