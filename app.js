/**
 * Created by Rainc on 14-12-9.
 */
var express = require('express');
var app = module.exports = express();
var config = require('./etc/config.json');
var fs = require('fs');
var exec = require('child_process').exec;
var appServer = require(config.appServerPath);
var fileCtrl = require('./controllers/fileWatch.js');
var router = require('./routers');
var log4js = require('log4js');
var bodyParser = require('body-parser');

log4js.configure({
  appenders: [
    { type: 'console' }, //控制台输出
    {
      type: 'dateFile', //日志文件输出
      filename: config.webLogFile,
      //     maxLogSize: 1024,
      backups: 3,
      pattern: "-yyyy-MM-dd",
      alwaysIncludePattern: false,
      category: 'WEBSERVER'
    }
  ],
  replaceConsole: true
});
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

//app.configure
app.use(log4js.connectLogger(logger, {level:'auto', format:':remote-addr :method :url'}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/public');
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);

//路由
router(app);

var pidFilePath = config.pidFilePath;
fs.open(pidFilePath,'a+',function(err){
  if(err){
    logger.error(err);
  }else{
    fs.readFile(pidFilePath, 'utf8', function (err, data) {
      if (err) {
        logger.error(err);
      }else if(!data){
        runApp();
      }else{
//            console.log(data);
        var cmd = "ps -ax | grep "+ data + " | awk '{ print $1 }'";
        var isPidActive = false;
        exec(cmd,
          function (error, stdout, stderr) {
            if (error !== null) {
              logger.error('exec error: ' + error);
            }else if(stdout){
//                  console.log(stdout);
              var result = stdout.split('\n');
              for(var i in result){
                if(data == result[i]){
                  isPidActive = true;
                  break;
                }
              }
//                  console.log(JSON.stringify(result));
              if(isPidActive == false){
                runApp();
              }else{
                logger.info("record server is running!Please stop it before to run another one!");
                process.exit();
              }
            }else{
              runApp();
            }
          });
      }
    })
  }
})

function runApp(){
  appServer.init(function(err,obj){
    if(!err){
      product_id = obj.product_id;
      server_id = obj.server_id;
      server_version = obj.server_version;
      recording_path = obj.recording_path || '';
      status_path = obj.status_path || '';
      backup_path = obj.backup_path || '';
//    server_version = 'v1.0';
//    server_id = 1000;
//    product_id = 61;
//    backup_path = 'D://document//recording_pack//';
//    recording_path = 'D://document';
//    status_path = 'D://document//status.txt';

      fileCtrl.saveToMemory();
      fileCtrl.watchFile();

      app.use(express.static(recording_path));
      app.use(express.static(backup_path));
      app.listen(config.web_port);     //启动web server
      logger.info("server listening on port %d in %s mode", config.web_port, app.settings.env);
      logger.info("You can debug your app with http://" + config.web_host + ':' + config.web_port);
    }else{
      logger.error(err);
    }
  });
}

