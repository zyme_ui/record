var dgram  = require('dgram');
var path = require('path');
var nconf = require('nconf');
var drecorder = require('./build/Debug/drecorder');
var async = require('async');
var NanoTimer = require('nanotimer');

var mtg_proto = require('./mtg_proto');
var net_utils = require('./net_utils');

var server = dgram.createSocket('udp4');
var sessions = {};
var call_sessions = {};

nconf.argv()
     .env()
     .file({ file: 'etc/config.json' })
     .defaults({'recording_path': '/tmp', 'log_path':'/tmp', 'server_ip':'127.0.0.1'});


var logger = require('./logger_factory')(nconf.get("log")):


var ip_uint32 = net_utils.addr2long(nconf.get("server_ip"));

function generate_session_id(call_id) {
  return ""+(new Date()).getTime()+"_"+call_id;
}

function build_session(session_id, call_id, caller, called, channels, sockets, peer_info) {
  if(sessions[session_id]) {
    throw "session exists";
  }
  var now = (new Date).getTime();
  var recv_timestamps = [];
  for(var i = 0; i < channels.length; ++i) {
    recv_timestamps.push(now);
  }
  var session = {id:session_id, call_id:call_id, caller:caller, called:called, channels:channels, chan_sockets:sockets, peer_info:peer_info
          ,recv_timestamps:recv_timestamps, first_packet:true};
  //wait for 5s before receive first packet
  session.wait_timer = setInterval(function() {
    if(!is_working(session)) {
      stop_record_when_error("[DRecord "+session_id+"] wait first pakcet too long!", session.id, session.call_id, 0);
    }
  }, 1000);
  sessions[session_id] = session;
}

function get_session(session_id) {
  return sessions[session_id];
}

function make_call_info(call_id, peer_info) {
  return peer_info.address+"_"+peer_info.port+"_"+call_id;
}

function remove_session(session) {
  var call_info = make_call_info(session.call_id, session.peer_info);
  delete sessions[session.id];
  delete call_sessions[call_info];
}

function get_session_by_peer(call_id, peer_info) {
  var call_info = make_call_info(call_id, peer_info);
  return call_sessions[call_info];
}

function make_out_path(session_id, caller, called) {
  return path.join(nconf.get("recording_path"), session_id+"_"+caller+"_"+called+".wav");
}

function stop_record_when_error(error, session_id, call_id, chan_id) {
  var session = get_session(session_id);
  if(!session) {
    return;
  }
  console.log(""+session_id+" stop_record_when_error "+error);
  if(error.stack) {
    console.log("stack:"+error.stack);
  }
  remove_session(session);
  if(session.recording_timer) {
    (session.recording_timer);
  }
  if(session.wait_timer) {
    clearInterval(session.wait_timer);
  }
  if(session.sockets) {
    session.sockets.forEach(function(socket) {
      socket.close();
    });
  }
  drecorder.stop_recorder(session_id);
}

//#WARNING: the implement assume chan_id can direct use as index
function update_work_time(session_id, chan_id) {
  var session = get_session(session_id);
  if(session) {
    session.recv_timestamps[chan_id] = (new Date).getTime();
  }
}

function is_working(session) {
  var deadline = (new Date).getTime() - 5000;
  console.log("[is_working] deadline:"+deadline+"    recv_timestamps:"+session.recv_timestamps);
  for(var i = 0; i < session.recv_timestamps.length; ++i) {
    if( session.recv_timestamps[i] > deadline ) {
      return true;
    }
  }
  console.log("[is_working] not working now!!");
  return false;
}

function get_millisecs(nano) {
  return nano[0]*1000+nano[1]/1000000;
}

function try_start_recording_timer(session_id, chan_id, method) {
  var session = get_session(session_id);
  if(session) {
    if(session.recording_timer) { //timer alread started
      return;
    }
    //start timer when received first media packet
    var timer = new NanoTimer();
    var ts = process.hrtime();
    var interval = 10;
    var interval_str = '10m';
    var stopped = false;
    var timer_fun;

    session.recording_timer = timer;
    var reschedule = function(info) {
      if(stopped) {
        console.log("record stopped");
        return;
      }
      var delta = Math.round(info.waitTime/1000000);
      var val; 
      if(delta > interval) {
        val = ''+((interval<<1)-delta)+'m';
      } else {
        val = interval_str;
      }

      timer.setTimeout(timer_fun, [], val, reschedule);
    };
    var _timer_fun = function() {
      drecorder.record(session_id, function(err) {
        if(err) {
          stopped = true;
          stop_record_when_error(err, session_id, session.call_id, chan_id);
        } else {
          //reschedule(timer_fun);
        }
      });
    };
    var _timer_fun_sync = function() {
      try {
        drecorder.record_sync(session_id);
        //reschedule(timer_fun_sync);
      } catch(err) {
        console.log("stop record:"+err.stack);
        stopped = true;
        stop_record_when_error(err, session_id, session.call_id, chan_id);
      }
    }
    if(method == "sync") {
      timer_fun = _timer_fun_sync;
    } else {
      timer_fun = _timer_fun;
    }
    timer.setTimeout(timer_fun, [], interval_str, reschedule);
  } else {
    console.log("SHOULD NOT REACH HERE! "+session_id);
  }
}

function start_channel_socket(session_id, call_id, chan_id, peer_info, bind_callback, method) {
  var socket = dgram.createSocket('udp4');
  var packet_count = 0;
  socket.on("error", function (err) {
    console.log("chan socket error:\n" + err.stack);
    if(err && err.syscall == "bind") {
      bind_callback(err); //socket bind error
      return;
    } else {
      if(socket) {
        socket.close();
      }
      socket = null;
      stop_recorder_when_error(err, session_id, call_id, chan_id);
      return;
    }
  });

  socket.on("message", function (msg, rinfo) {
    /*console.log("got media("+msg.length+"B) at " +*/
      /*rinfo.address + ":" + rinfo.port);*/
    if(rinfo.address != peer_info.address) {
      //console.log("discard packet because unknown source");
      return;
    }
    if(msg.length != mtg_proto.SIZE_MEDIA_PACKET) {
      //console.log("discard packet because size incorrect:"+msg.length);
      return;
    } 
    try {
      if(method == "sync") {
        try {
          drecorder.channel_on_data_sync(session_id, chan_id, msg);
          update_work_time(session_id, chan_id);
        } catch(error) {
          console.log(session_id+" channel_on_data_sync "+chan_id+" error "+error);
        }
      } else {
        drecorder.channel_on_data(session_id, chan_id, msg, function(error) {
          if(error) {
            console.log(session_id+" channel_on_data "+chan_id+" error "+error);
          } else {
            update_work_time(session_id, chan_id);
          }
        });
      }

      if(!packet_count) {
        var session = get_session(session_id);
        if(session && session.first_packet) {
          session.first_packet = false;
          try_start_recording_timer(session_id, chan_id, method);
        }
      }
      packet_count++;
    } catch(ex) {
      console.log(""+ex+","+ex.stack);
    }
  });

  socket.on("listening", function () {
    var address = socket.address();
    console.log("chan socket listening " +
        address.address + ":" + address.port);
    bind_callback(null, socket);
  });

  socket.bind(0); //bind dynamic port
}

///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复<]

    //S8              szCalling[RCD_MAX_NUM_LEN+1];
    //S8              szCalled[RCD_MAX_NUM_LEN+1];

//}RECORD_MSG_S;
///////////////////////////////////////////////////////////////////////////////
function maybe_record_start(msg, rinfo) {
  var req = mtg_proto.parse_record_start(msg);
  var session_id = generate_session_id(req.call_id);

  console.log("received record_start " + JSON.stringify(req) + ", session id is " + session_id);

  try {
    drecorder.start_recorder(session_id, req.caller, req.called, {'out_dir':make_out_path(session_id, req.caller, req.called), 'format':"wav"});
    //create 2 channels for caller->called and called->caller
    console.log("add channel");
    var channels = drecorder.add_channels(session_id, 2, {});
    console.log("start sockets for channels:"+channels);
  } catch(ex) {
    console.log("exception:"+ex.stack);
  }
  
  var caller_port = 0, called_port = 0;
  async.map(channels, 
      function(chan_id, callback) {
        console.log("start channel socket "+chan_id);
        start_channel_socket(session_id, req.call_id, chan_id, rinfo, function(error, socket) {
          if(error) {
            callback(error);
          } else {
            callback(null, socket);
          }
        });
      }, 
      function(error, sockets) {
        var ack;
        if(error) {
          console.log("start channel socket error "+error);
          drecorder.stop_recorder(session_id);
          if(sockets) { //close opened channel socket 
            sockets.forEach(function(s) {
              s.close();
            });
          }
          ack = mtg_proto.make_record_start_ack(req.sn, req.call_id, mtg_proto.ERR_RECORD_SERV_UNAVA);
        } else {
          build_session(session_id, req.call_id, req.caller, req.called, channels, sockets, rinfo);
          ack = mtg_proto.make_record_start_ack(req.request_id, req.call_id, mtg_proto.ERR_SUCCESS
              ,sockets[0].address().port, sockets[1].address().port, ip_uint32);
        }
        //first socket for caller->called, second socket for called->caller
        server.send(ack, 0, ack.length, rinfo.port, rinfo.address, function(error) {
          if(error) {
            console.log("send ack to " + rinfo.address + ":" + rinfo.port + " error "+error);
          }
        });
    });
}


///////////////////////////////////////////////////////////////////////////////
//
//typedef struct
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复,  此处不关心, 置0<]
//}LNK_DETECT_MSG_S;
///////////////////////////////////////////////////////////////////////////////
function maybe_link_detect(msg, rinfo) {
  var req = mtg_proto.parse_link_detect(msg);
  if(get_session_by_peer(req.call_id, rinfo)) {
    ack = mtg_proto.make_link_detect_ack(req.request_id, req.call_id);
    server.send(ack, 0, buffer.length, rinfo.address, rinfo.port, function(error) {
      if(error) {
        console.log("send ack to " + rinfo.address + ":" + rinfo.port + " error "+error);
      }
    });
  }
}


server.on("error", function (err) {
  console.log("server error:\n" + err.stack);
  server.close();
});

server.on("message", function (msg, rinfo) {
  console.log("server got: " + " from " +
    rinfo.address + ":" + rinfo.port);
  try {
    if(msg.length == mtg_proto.SIZE_CMD_RECORD_START) {
      maybe_record_start(msg, rinfo);
    } else if(msg.length == mtg_proto.SIZE_CMD_LNK_DETECT) {
      maybe_link_detect(msg, rinfo);
    }
    else {
      console.log("invalid message");
      return;
    }
  } catch(ex) {
    console.log("handle message failed:"+ex.stack);
  }
});

drecorder.init(path.join(nconf.get("log_path"), "drecorder.log"), 0);
server.on("listening", function () {
  var address = server.address();
  console.log("server listening " +
      address.address + ":" + address.port);
});

setInterval(function() {
  var stats = drecorder.internal_status();
  if(stats) {
    console.log("internal status:"+JSON.stringify(drecorder.internal_status()));
  }
}, 1000);

//start server and run loop
server.bind(nconf.get("server_port"), nconf.get("server_ip"));
  
