var _  = require('underscore');

function MTGProto() {

//command define
this.CMD_RECORD_START = 0;
this.CMD_RECORD_START_ACK = 1;
this.CMD_RECORD_STOP = 2;
this.CMD_RECORD_STOP_ACK = 3;
this.CMD_LNK_DETECT = 4;
this.CMD_LNK_DETECT_ACK = 5;

//error code define
this.ERR_SUCCESS = 0;
this.ERR_RECORD_SERV_UNAVA = 1;
this.ERR_RECORD_NO_SERV_RIGHTS = 2;

//command struct size
this.RCD_MAX_NUM_LEN = 32;
this.SIZE_CMD_RECORD_START = 4 + this.RCD_MAX_NUM_LEN*2;
this.SIZE_CMD_RECORD_START_ACK = 16;
this.SIZE_CMD_RECORD_STOP = this.SIZE_CMD_RECORD_START;
this.SIZE_CMD_RECORD_STOP_ACK = this.SIZE_CMD_RECORD_START_ACK;
this.SIZE_CMD_LNK_DETECT = 4;
this.SIZE_CMD_LNK_DETECT_ACK = 4;
this.SIZE_MEDIA_PACKET_HEADER = 10;
this.SIZE_MEDIA_PACKET = 170;


this.request_id = function(msg) {
  return msg.length > 0 ? msg.readUInt8(1) : undefined;
}

this.call_id = function(msg) {
  return msg.length > 3 ? msg.readUInt16BE(2) : undefined;
}


///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复<]

    //S8              szCalling[RCD_MAX_NUM_LEN+1];
    //S8              szCalled[RCD_MAX_NUM_LEN+1];

//}RECORD_MSG_S;
///////////////////////////////////////////////////////////////////////////////
this.make_record_start = function (request_id, call_id, caller, called) {
  if(!caller || !_.isString(caller)) {
    throw "invalid caller";
  }
  if(!called || !_.isString(called)) {
    throw "invalid called";
  }
  if(caller.length >= this.RCD_MAX_NUM_LEN) {
    throw "caller too long";
  }
  if(called.length >= this.RCD_MAX_NUM_LEN) {
    throw "called too long";
  }

  var buffer = new Buffer(this.SIZE_CMD_RECORD_START);
  var offset = 0;
  buffer.writeUInt8(this.CMD_RECORD_START, offset++);
  buffer.writeUInt8(request_id, offset++);
  buffer.writeUInt16BE(call_id, offset);
  offset += 2;
  buffer.write(caller, offset);
  buffer.writeUInt8(0, offset+caller.length);
  offset += this.RCD_MAX_NUM_LEN;
  buffer.write(called, offset);
  buffer.writeUInt8(0, offset+called.length);
  return buffer;
}

this.buffer_read_str = function (buffer, start, max_offset) {
  while(buffer[start] && ++start < max_offset);
  return start;
}

this.parse_record_start = function (msg) {
  var offset = 0;
  var type = msg.readUInt8(offset++);
  if(type == this.CMD_RECORD_START) {
    var sn = msg.readUInt8(offset++);
    var call_id = msg.readUInt16BE(offset);
    offset += 2;
    var end = this.buffer_read_str(msg, offset, offset+this.RCD_MAX_NUM_LEN); 
    var caller = msg.slice(offset, end).toString();
    offset += this.RCD_MAX_NUM_LEN;
    end = this.buffer_read_str(msg, offset, offset+this.RCD_MAX_NUM_LEN);
    var called = msg.slice(offset, end).toString();
    return {"request_id":sn, "call_id":call_id, "caller":caller, "called":called};
  } else {
    throw new TypeError("invalid msg type "+type);
  }
}


///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;          [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复<]

    //U8              ucErrCode;		    [>  错误码, 用来标志响应结果<]
    //U8              ucReserve1;         [>  保留<]
    //U16             usReserve2;         [>  保留<]
    //U16             usRcvTDMRxUdpPort;  [>  录音服务器接收TDM Rx 录音的端口<]
    //U16             usRcvTDMTxUdpPort;  [>  录音服务器接收TDM Tx 录音的端口<]
    //U32             ulRcvIP;            [>  录音服务器接收录音的IP <]

//}RECORD_MSG_ACK_S;
///////////////////////////////////////////////////////////////////////////////
this.make_record_start_ack = function (sn, call_id, error, caller_port, called_port, ms_ip) {
  var buffer = new Buffer(this.SIZE_CMD_RECORD_START_ACK);
  var offset = 0;
  buffer.writeUInt8(this.CMD_RECORD_START_ACK, offset++);
  buffer.writeUInt8(sn, offset++);
  buffer.writeUInt16BE(call_id, offset);
  offset += 2;
  if(error) {
    buffer.writeUInt8(this.ERR_RECORD_SERV_UNAVA, offset++);
    return buffer;
  } else {
    buffer.writeUInt8(this.ERR_SUCCESS, offset++);
  }
  offset += 3; //skip reserve fields
  buffer.writeUInt16BE(caller_port, offset);
  offset += 2;
  buffer.writeUInt16BE(called_port, offset);
  offset += 2;
  buffer.writeUInt32BE(ms_ip, offset);
  return buffer;
}

this.parse_record_start_ack = function (msg) {
  var offset = 0;
  var type = msg.readUInt8(offset++);
  if(type == this.CMD_RECORD_START_ACK) {
    var sn = msg.readUInt8(offset++);
    var call_id = msg.readUInt16BE(offset);
    offset += 2;
    var error = msg.readUInt8(offset++);
    offset += 3; //skip reserve
    var caller_port = msg.readUInt16BE(offset);
    offset += 2;
    var called_port = msg.readUInt16BE(offset);
    offset += 2;
    var ms_ip = msg.readUInt32BE(offset);
    return {"request_id":sn, "call_id":call_id, "caller_port":caller_port, "called_port":called_port, "ms_ip":ms_ip};
  } else {
    throw new TypeError("invalid msg type "+type);
  }
}

///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复<]

    //S8              szCalling[RCD_MAX_NUM_LEN+1];
    //S8              szCalled[RCD_MAX_NUM_LEN+1];

//}RECORD_STOP_MSG_S;
///////////////////////////////////////////////////////////////////////////////
this.make_record_stop = function (request_id, call_id) {
  if(!caller || !_.isString(caller)) {
    throw new TypeError("invalid caller");
  }
  if(!called || !_.isString(called)) {
    throw new TypeError("invalid called");
  }

  var buffer = new Buffer(this.SIZE_CMD_RECORD_STOP);
  var offset = 0;
  buffer.writeUInt8(this.CMD_RECORD_STOP, offset++);
  buffer.writeUInt8(request_id, offset++);
  buffer.writeUInt16BE(call_id, offset);
  return buffer;
}

this.parse_record_stop = function (msg) {
  var offset = 0;
  var type = msg.readUInt8(offset++);
  if(type == this.CMD_RECORD_STOP) {
    var sn = msg.readUInt8(offset++);
    var call_id = msg.readUInt16BE(offset);
    offset += 2;
    return {"request_id":sn, "call_id":call_id};
  } else {
    throw new TypeError("invalid msg type "+type);
  }
}

///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;          [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复<]

    //U8              ucErrCode;		    [>  错误码, 用来标志响应结果<]
    //U8              ucReserve1;         [>  保留<]
    //U16             usReserve2;         [>  保留<]
    //U16             usRcvTDMRxUdpPort;  [>  录音服务器接收TDM Rx 录音的端口<]
    //U16             usRcvTDMTxUdpPort;  [>  录音服务器接收TDM Tx 录音的端口<]
    //U32             ulRcvIP;            [>  录音服务器接收录音的IP <]

//}RECORD_STOP_MSG_ACK_S;
///////////////////////////////////////////////////////////////////////////////
this.make_record_stop_ack = function (sn, call_id) {
  var buffer = new Buffer(this.SIZE_CMD_RECORD_STOP_ACK);
  var offset = 0;
  buffer.writeUInt8(this.CMD_RECORD_STOP_ACK, offset++);
  buffer.writeUInt8(sn, offset++);
  buffer.writeUInt16BE(call_id, offset);
  return buffer;
}

this.parse_record_stop_ack = function (msg) {
  var offset = 0;
  var type = msg.readUInt8(offset++);
  if(type == this.CMD_RECORD_START_ACK) {
    var sn = msg.readUInt8(offset++);
    var call_id = msg.readUInt16BE(offset);
    return {"request_id":sn, "call_id":call_id};
  } else {
    throw new TypeError("invalid msg type "+type);
  }
}

///////////////////////////////////////////////////////////////////////////////
//
//typedef struct
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复,  此处不关心, 置0<]
//}LNK_DETECT_MSG_S;
///////////////////////////////////////////////////////////////////////////////
this.make_link_detect = function (request_id, call_id) {
  var buffer = new Buffer(this.SIZE_CMD_LNK_DETECT);
  var offset = 0;
  buffer.writeUInt8(request_id, offset++);
  buffer.writeUInt16BE(call_id, offset);
  return buffer;
}

this.parse_link_detect = function (msg) {
  var offset = 0;
  var type = msg.readUInt8(offset++);
  if(type == this.CMD_LNK_DETECT) {
    var sn = msg.readUInt8(offset++);
    var call_id = msg.readUInt16BE(offset);
    return {"request_id":sn, "call_id":call_id};
  } else {
    throw new TypeError("invalid msg type "+type);
  }
}

///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U8              ucMsgType;         [>  消息类型<]    
    //U8              ucMsgSn;           [>  消息序号<]
    
    //U16             usCallId;		   [>  呼叫标示, 短时间内不会重复,  此处不关心, 置0<]
/*}LNK_DETECT_MSG_ACK_S;*/
///////////////////////////////////////////////////////////////////////////////
this.make_link_detect_ack = function (sn, call_id) {
  var buffer = new Buffer(this.SIZE_CMD_LNK_DETECT_ACK);
  var offset = 0;
  buffer.writeUInt8(this.CMD_LNK_DETECT_ACK, offset++);
  buffer.writeUInt8(sn, offset++);
  buffer.writeUInt16BE(call_id, offset);
  return buffer;
}

///////////////////////////////////////////////////////////////////////////////
//
/*typedef struct*/
//{
    //U32             magic;         
    //U16             ucMsgSn;      
    //U32             timestamp      
    
    //U8             payload[160];
/*}audio_packet;*/
///////////////////////////////////////////////////////////////////////////////
this.make_audio_packet = function (timestamp, seq, payload) {
  var buffer = new Buffer(this.SIZE_MEDIA_PACKET_HEADER);
  var offset = 0;
  buffer.writeUInt16LE(0, offset);
  offset += 2;
  buffer.writeUInt16LE(0x255, offset);
  offset += 2;
  buffer.writeUInt16LE(seq, offset);
  offset += 2;
  buffer.writeUInt16LE((timestamp&0xffff0000)>>>16, offset);
  offset += 2;
  buffer.writeUInt16LE(timestamp&0x0000ffff, offset);
  offset += 4;
  return Buffer.concat([buffer, payload]);
}

}

module.exports = new MTGProto;

/*module.exports.make_record_start = make_record_start;*/
//module.exports.make_record_start_ack = make_record_start_ack;
//module.exports.make_link_detect = make_link_detect;
//module.exports.make_link_detect_ack = make_link_detect_ack;
//module.exports.make_audio_packet = make_audio_packet;

//module.exports.parse_record_start = parse_record_start;
/*module.exports.parse_record_start_ack = parse_record_start_ack;*/
//module.exports.parse_link_detect = parse_link_detect;
//module.exports.parse_link_detect_ack = parse_link_detect_ack;
//module.exports.parse_audio_packet = parse_audio_packet;
