#录音服务

###状态写入到文件, 文件定义在etc/config.json的status_path字段
###状态内容包括"产品信息"、"服务配置"、"系统状态"、"服务状态"、"引擎状态"
例子:
{"engine_stats":{"audio_time":0,"mix_time":0,"write_time":0,"record_time":0,"fraction_lost":0,"cumulative_lost":0,"extended_max_sequence_number":0,"jitter":0,"current_buffer_size_ms":0,"preferred_buffer_size_ms":0,"jitter_peaks_found":0,"packet_loss_rate":0,"packet_discard_rate":0,"expand_rate":0,"preemptive_rate":0,"accelerate_rate":0,"clockdrift_ppm":0,"added_zero_samples":0},"app_status":{"sessions_count":0},"product_info":{"product_id":"61","product_name":"DRSERVER","server_id":"123456"},"runtime":{"system_uptime":53291.48713298,"process_id":5781,"process_uptime":0,"total_memory":1014268,"free_memory":456884,"loadavg_1m":0.29296875,"loadavg_5m":1.46484375,"loadavg_15m":4.541015625,"rx_rate":1.8,"tx_rate":0.4,"disk_used":340062,"disk_available":136219},"app_config":{"listen_ip":"10.211.55.14","listen_port":3000,"max_sessions":60}}

####product_info里包含的信息
产品ID－－product_id
产品名称－－product_name
服务ID－－server_id

####runtime里包含的信息
系统启动天数－－system_uptime, 单位是秒, 需要换算下
服务启动天数－－process_uptime
内存占用率 －－free_memory/total_memory, 单位KB
CPU使用率－－loadavg_1m*100, 单位 %
网络接收流量－－rx_rate, 单位KBPS
网络发送流量－－tx_rate，单位KBPS
录音磁盘占用率－－disk_used/(disk_used+disk_avaiable)
录音文件占用空间－－disk_used, 单位KB

####app配置和状态里包含的信息
最大录音会话数－－max_sessions
当前录音会话数－－sessions_count
服务IP－－listen_ip
服务端口－－listen_port

####engine_stats，debug用
