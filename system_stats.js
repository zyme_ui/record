///////////////////////////////////////////////////////////////////////////////
// system_stats
// gather system stats. can use filter for specific items.
// now filter support bind ip and working directory.
// system_stats collect stats period, when collected then fire two events: 
//  event("error", reason) --> when collect error, fire event with error reason.
//  event("collected", info) --> when collect over, fire event with stats info.
// now stats info include these items:
//  {system_uptime: SYSTEM_UPTIME //seconds
//      , process_id: PID
//      , process_uptime: PROCESS_UPTIME //seconds
//      , total_memory: TOTAL_MEMORY //bytes
//      , free_memory:os.freemem()  //bytes
//      , tx_rate: rate //kbps
//      , rx_rate: rate //kbps
//      , used: used_mount //kb
//      , available: available //kb
//  }
//
//
//
///////////////////////////////////////////////////////////////////////////////


var os = require('os');
var events = require('events');
var exec = require('child_process').exec;
var fs = require('fs');

var async = require('async');
var _ = require('underscore');

const SYSTEM_UPTIME = new Date().getTime() - Math.round(os.uptime()*1000);
const PID = process.pid;
const PROCESS_UPTIME = new Date().getTime() - Math.round(process.uptime()*1000);
const TOTAL_MEMORY = os.totalmem() / (1024*1024);
const CPU_COUNT = os.cpus().length;

var g_last_tx_bytes = null, g_last_rx_bytes = null;


function get_dir_mount(dir, callback) {
  var command = "df -P "+dir;
  exec(command, function(error, stdout, stderr) { 
    if(error) {
      callback(error);
    } else if(stderr) {
      callback(stderr);
    } else {
      var lines = stdout.split("\n");
      var info = {disk_used:0, disk_available:0};
      if(lines.length > 1) {
          var sline = lines[1].trim().replace(/\s+/g, ":");
          var aline = sline.split(":");
          if(aline[5]) {
            info.disk_used = (+aline[2]) / 1024; //kb
            info.disk_available = (+aline[3]) / 1024; //kb
          }
      } 
      callback(null, info);
    }
  });
}

function get_traffic(interval, bind_if, callback) {
  var secs = interval/1000;
  fs.readFile('/proc/net/dev', 'utf-8', function(error, stats) {
    if (error) {
      callback(error);
    } else {
      var lines = stats.split("\n");
      var info = {rx_rate:0, tx_rate:0};
      for(var i=0;i<lines.length; i++) {
        var aline = lines[i].split(':');
        if(!aline[1]) continue;
        var key = aline.shift();
        key = key.trim();
        aline = aline[0].trim().replace(/\s+/g, ":");
        aline = aline.split(":");
        if(key === bind_if) { 
          if(g_last_rx_bytes != null) {
            info.rx_rate = ((+aline[0]-g_last_rx_bytes) / 1024)/secs; //kbps
            info.tx_rate = ((+aline[8]-g_last_tx_bytes) / 1024)/secs; //kbps
          }
          g_last_rx_bytes = +aline[0];
          g_last_tx_bytes = +aline[8];
        } 
        callback(null, info);
      }
    }
  });
}

function find_if_by_ip(ip) {
  var interfaces = os.networkInterfaces();
  for(var netif in interfaces) {
    var ifdesc = interfaces[netif];
    for(var i = 0; i < ifdesc.length; ++i) {
      if(ifdesc[i].address == ip)  {
        return netif;
      }
    }
  }
}

function gather(interval, bind_if, used_dir, callback) {
  var cpu_usage = os.loadavg();
  var info = {system_uptime: SYSTEM_UPTIME
    , process_id: PID
    , process_uptime: PROCESS_UPTIME
    , total_memory: TOTAL_MEMORY
    , free_memory:os.freemem() / (1024*1024) 
    , loadavg_1m: (cpu_usage[0]/CPU_COUNT)*100
    , loadavg_5m: (cpu_usage[1]/CPU_COUNT)*100
    , loadavg_15m: (cpu_usage[2]/CPU_COUNT)*100
  };

  async.waterfall([
    function(cb) {
      get_traffic(interval, bind_if, cb);
    },
    function(traffic, cb) {
      _.extend(info, traffic);
      get_dir_mount(used_dir, cb);
    }
  ], function(error, mount_info) {
    if(error) {
      callback(error);
    } else {
      _.extend(info, mount_info);
      callback(null, info);
    }
  });
}

function start(interval, filter) {
  var emitter = new events.EventEmitter;
  var bind_if = find_if_by_ip(filter.ip);
  setInterval(function() {
    gather(interval, bind_if, filter.work_dir, function(error, info) {
      if(error) {
        emitter.emit("error", error);
      } else {
        emitter.emit("collected", info);
      }
    });
  }, interval);
  return emitter;
}

exports.start = start;
