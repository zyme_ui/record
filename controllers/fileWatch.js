/**
 * Created by Rainc on 15-3-3.
 */
var fs = require('fs');
var async = require('async');
var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

//获取文件信息，存储在内存中
exports.saveToMemory = function(){
  fs.readdir(recording_path,function(err,files){
    if(err){
      logger.error(err);
    }else{
      var arr = [];
      for(var i in files){
        arr.push({fileName: files[i],filePath:''})
      }
      GLOBAL.recordDirList = arr;  //目录信息
      GLOBAL.recordList = {};  //录音文件信息
      async.forEach(files,function(items,cb){
        var dirPath = recording_path + "//" + items + '//';
        fs.readdir(dirPath,function(err,file){
          if(err){
            logger.error(err);
          }else{
            var arr = [];
            async.forEach(file,function(item,cb){
              fs.stat(dirPath + item,function(err,stats){
                if(stats){
                  var name = item.split('_');
                  var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                  var MTG_PORT = name[5];
                  var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                  var caller = name[8];
                  var called = name[9].split('.')[0];
//                        item = name[0] + '.wav'
                  arr.push({fileName: item,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:items,parentFileName:items,fileCtime:format(new Date().setTime(stats.ctime),'yyyyMMddHHmmss'),fileSize:parseInt(stats.size/1024)})
                }
                cb(null);
              })
            },function(err){
              GLOBAL.recordList[items] = arr;
              cb(null);
            })
          }
        })
      },function(err){
        if(err == null){
          console.log('文件信息缓存成功！');
//        console.log(GLOBAL.recordList,GLOBAL.recordDirList);
        }
      })
    }
  })
}

//启动监听文件改动程序
exports.watchFile = function(){
  fs.watch(recording_path, function (event, filename) {
    console.log('event is: ' + event);
    if(event == 'change'){
      if(filename){
        var dirPath = recording_path + "//" + filename + '//';
        fs.readdir(dirPath,function(err,file){
          if(err){
            logger.error(err);
          }else{
            var arr = [];
            async.forEach(file,function(item,cb){
              fs.stat(dirPath + item,function(err,stats){
                if(stats){
                  var name = item.split('_');
                  var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                  var MTG_PORT = name[5];
                  var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                  var caller = name[8];
                  var called = name[9].split('.')[0];
//                        item = name[0] + '.wav'
                  arr.push({fileName: item,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:filename,parentFileName:filename,fileCtime:format(new Date().setTime(stats.ctime),'yyyyMMddHHmmss'),fileSize:parseInt(stats.size/1024)})
                }
                cb(null);
              })
            },function(err){
              GLOBAL.recordList[filename] = arr;
              console.log(GLOBAL.recordList);
            })
          }
        })
      }
    }
  });
}

function format(time, format) {
  var a = new Date(time);
  a.setTime(a.getTime() - 28800000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) + '-';
        break;
      case 'MM':
        return tf(t.getMonth() + 1) + '-';
        break;
      case 'mm':
        return tf(t.getMinutes()) + ':';
        break;
      case 'dd':
        return tf(t.getDate()) + ' ';
        break;
      case 'HH':
        return tf(t.getHours()) + ':';
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}