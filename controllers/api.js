/**
 * Created by Rainc on 15-1-6.
 */

var fs = require('fs');
//var loadCache = require('../appServer');
var sendTo= require('../sendToRemote');
var async = require('async');

var log4js = require('log4js');
var logger = log4js.getLogger('WEBSERVER');
logger.setLevel('INFO');

exports.recordApi = function(req,res){
  var action = req.query.action;
  if(!action || action == ''){
    res.json({success:false,code:"require param action!"})
  }else{
//    fs.open(status_path,'r',function(err){
//      if(err){
//        logger.error(err);
//        res.json({success:false,code:err});
//      }else{
//        fs.readFile(status_path, 'utf8', function (err, data) {
//          if (err) {
//            logger.error(err);
//            res.json({success:false,code:err})
//          }else if(data){
//            try{
//              data = JSON.parse(data);
//            }catch(e){
//              logger.error(e);
//              res.json({success:false,code:e})
//              return;
//            }
    var cache = [];
    var data = GLOBAL.g_app_status;
    var num = 0;
    for(var i in data){
      num++;
    }
    if(num != 0){
      try{
        switch(action){
          case 'getStatus':
            data.timestamp = format(data.timestamp,'yyyyMMddHHmmss');
            data.runtime.system_uptime = format(data.runtime.system_uptime,'yyyyMMddHHmmss');
            data.runtime.process_uptime = format(data.runtime.process_uptime,'yyyyMMddHHmmss');
            data.runtime.memoryUtilization = parseInt((data.runtime.free_memory/data.runtime.total_memory)*100) ;    //内存占用率
            data.runtime.total_memory = parseInt(data.runtime.total_memory) ;
            data.runtime.loadavg_1m = parseInt(data.runtime.loadavg_1m) ;   //CPU使用率
//            data.runtime.rx_rate =  data.runtime.rx_rate ;
//            data.runtime.tx_rate =  data.runtime.tx_rate ;
            data.runtime.diskUtilization = parseInt(parseInt(data.runtime.disk_used)/(parseInt(data.runtime.disk_used) + parseInt(data.runtime.disk_available))*100); //录音磁盘占用率
//            data.runtime.disk_used = data.runtime.disk_used ;
            res.json({success:true,code:data});
            break;
          case 'sendToCloud':
            sendTo.reSendTo(function(err){
              if(err){
              }else{
                res.json({success:true,code:0})
              }
            })
            break;
          case 'getRecordList':
            var dir = req.query.dir;
            var dirPath = '';
            if(dir){
              if(GLOBAL.recordList[dir]){
                res.json(GLOBAL.recordList[dir])
              }
//              dirPath = recording_path + "//" +  dir + '//';
//              fs.readdir(dirPath,function(err,files){
//                if(err){
//                  logger.error(err);
//                }else{
//                  var arr = [];
//                  async.forEachSeries(files,function(item,cb){
//                    fs.stat(dirPath + item,function(err,stats){
//                      if(stats){
//                        var name = item.split('_');
//                        var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
//                        var MTG_PORT = name[5];
//                        var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
//                        var caller = name[8];
//                        var called = name[9].split('.')[0];
////                        item = name[0] + '.wav'
//                        arr.push({fileName: item,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:dir,parentFileName:dir,fileCtime:format(new Date().setTime(stats.ctime),'yyyyMMddHHmmss'),fileSize:parseInt(stats.size/1024)})
//                      }
//                      cb(null)
//                    })
//                  },function(err){
//                    if(!err){
//                      res.json(arr);
//                    }
//                  })
//                }
//              })
            }else{
              res.json(GLOBAL.recordDirList);
//              dirPath = recording_path;
//              fs.readdir(dirPath,function(err,files){
//                if(err){
//                  logger.error(err);
//                }else{
//                  var arr = [];
//                  for(var i in files){
//                    arr.push({fileName: files[i],filePath:''})
//                  }
////                      console.log(arr);
//                  res.json(arr)
//                }
//              })
            }
            break;
          case 'getRecordPack':
            dirPath = backup_path;
            fs.readdir(dirPath,function(err,files){
              if(err){
                logger.error(err);
              }else{
                var arr = [];
                async.forEachSeries(files,function(item,cb){
                  fs.stat(dirPath + item,function(err,stats){
                    if(stats){
                      var name = item.split('_');
                      var MTG_IP = name[1] + '.' + name[2] + '.' + name[3] + '.' + name[4];
                      var MTG_PORT = name[5];
                      var MTG_ADDRESS = MTG_IP + ":" + MTG_PORT;
                      var caller = name[8];
                      var called = name[9].split('.')[0];
//                        item = name[0] + '.wav'
                      arr.push({fileName: item,MTG_ADDRESS:MTG_ADDRESS,caller:caller,called:called,filePath:'',fileCtime:format(new Date().setTime(stats.ctime),'yyyyMMddHHmmss'),fileSize:parseInt(stats.size/1024)})
                    }
                    cb(null)
                  })
                },function(err){
                  if(!err){
                    res.json(arr);
                  }
                })
              }
            })
            break;
          case 'download':
            var filePath = req.query.filePath;
            var fileName = req.query.fileName;
            if(filePath == ''){
              res.download(backup_path  + '/' + fileName);
            }else{
              res.download(recording_path + '/' + filePath + '/' + fileName);
            }
            break;
          case 'deleteFile':
            var fileName = req.query.fileName;
            var filePath = req.query.filePath;
            if(!fileName){
              res.json({success:false,code:'Lack Data(fileName)'})
            }else{
              if(filePath != ''){
                fs.unlink(recording_path + '/' + filePath + '/' + fileName,function(err){
                  if(err){
                    logger.error(err);
                    res.json({success:false,code:err})
                  }else{
                    res.json({success:true,code:0})
                  }
                })
              }else{
                fs.unlink(backup_path + '/' + fileName,function(err){
                  if(err){
                    logger.error(err);
                    res.json({success:false,code:err})
                  }else{
                    res.json({success:true,code:0})
                  }
                })
              }
            }
            break;
          case 'saveSetting':
//            var listen_ip = req.body.listen_ip;
//            var listen_port = req.body.listen_port;
            var server_version = req.body.server_version;
            var server_id = req.body.server_id;
            var product_name = req.body.product_name;
            var product_id = req.body.product_id;
            if(!server_version ||!server_id ||!product_name || !product_id){
              res.json({success:false,code:'Lack of incoming parameters'})
            }else{
//              GLOBAL.g_app_status.listen_ip = listen_ip;
//              GLOBAL.g_app_status.listen_port = listen_port;
              GLOBAL.g_app_status.server_version = server_version;
              GLOBAL.g_app_status.server_id = server_id;
              GLOBAL.g_app_status.product_name = product_name;
              GLOBAL.g_app_status.product_id = product_id;
              res.json({success:true,code:0})
            }
            break;
          default:
            res.json({success:false,code:'not valid action'})
        }
      }catch(err){
        logger.error(err);
        res.json({success:false,code:err})
      }
    }
//          }
//        });
//      }
//    })
  }
}

var format = function (time, format) {
  var a = new Date(time);
//  a.setTime(a.getTime() - 28800000);
  var t = new Date(a.getTime());
  var tf = function (i) {
    return (i < 10 ? '0' : '') + i
  };
  return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
    switch (a) {
      case 'yyyy':
        return tf(t.getFullYear()) + '-';
        break;
      case 'MM':
        return tf(t.getMonth() + 1) + '-';
        break;
      case 'mm':
        return tf(t.getMinutes()) + ':';
        break;
      case 'dd':
        return tf(t.getDate()) + ' ';
        break;
      case 'HH':
        return tf(t.getHours()) + ':';
        break;
      case 'ss':
        return tf(t.getSeconds());
        break;
    }
  })
}