var dgram  = require('dgram');
var fs = require('fs');
var events = require('events');
var async = require('async');
var wav = require('wav');
var nconf = require('nconf');
var NanoTimer = require('nanotimer');

var mtg_proto = require('./mtg_proto');
var net_utils = require('./net_utils');

nconf.argv().env()
  .defaults({"server_ip":"localhost", "lost_rate":0, "count":1});

const DR_CLIENT_STATE_INIT = 0;
const DR_CLIENT_STATE_INVITE = 1;
const DR_CLIENT_STATE_PLAY = 2;

const DR_SAMPLES_PER_SECONDS = 8000;
const DR_PACKET_TIME_STR = '20m';
const DR_PACKET_TIME = 20;
const DR_PACKET_SIZE = DR_SAMPLES_PER_SECONDS*20/1000;

const ERROR_LOST = 0xffff;

function DRChannel(args) {
  this.chan_id = args.chan_id;
  this.peer_ip = args.peer_ip;
  this.peer_port = args.peer_port;
  this.data = args.data;
  this.send_hook = args.send_hook;
  this.timer_hook = args.timer_hook;
  this.playing = true;
  this.timer = new NanoTimer();
  this.seq = 0;
  this.offset = 0;
  //this.timestamp = 0;
  this.socket = dgram.createSocket('udp4');
  var THIS = this;
  if(this.send_hook) {
    this.send = function(buf, offset, size, port, ip, callback) {
      THIS.send_hook(THIS.socket, buf, offset, size, port, ip, callback);
    } 
  } else {
    this.send = function(buf, offset, size, port, ip, callback) {
      console.log(THIS.socket.address().address+":"+THIS.socket.address().port+" send packet to "+ip+":"+port+" packet.length"+size);
      THIS.socket.send(buf, offset, size, port, ip, callback);
    }
  }
}

function get_millisecs(nano) {
  return (nano[0]*1000+nano[1]/1000000)&0xffffffff;
}

DRChannel.prototype.play_when_ready = function(callback) {
  this.socket.bind(0, function() {
    callback();
  });
}

//play a frame
DRChannel.prototype._play = function() {
  var THIS = this;
  var data = this.data;
  var offset = this.offset;
  var len = (offset + DR_PACKET_SIZE) < data.length && DR_PACKET_SIZE || (data.length-offset);
  if(len <= 0) {
    THIS.playing = false;
    return;
  }
  var packet = mtg_proto.make_audio_packet(get_millisecs(process.hrtime()), this.seq, data.slice(offset, offset+len));
  //var packet = mtg_proto.make_audio_packet(offset, this.seq, data.slice(offset, offset+len));
  this.send(packet, 0, packet.length, this.peer_port, this.peer_ip, function(error) {
    if(error && error != ERROR_LOST) {
      console.log(THIS.chan_id+" "+(new Date()).getTime()+" send audio packet error:"+error);
      THIS.playing = false;
    } else {
      //console.log(THIS.socket.address().address+":"+THIS.socket.address().port+" send packet to "+THIS.peer_ip+":"+THIS.peer_port+" packet.length"+len);
      if(error) {
        console.log(THIS.chan_id+" "+(new Date()).getTime()+" "+offset+" lost!!");
      } else {
        console.log(THIS.chan_id+" "+(new Date()).getTime()+" "+offset);
      }
      //this.timestamp += DR_PACKET_SIZE;
      THIS.offset += len;
      THIS.seq += 2;
      if(THIS.offset >= data.length) {
        THIS.playing = false;
      }
    }
    
  });
};

//play all data
DRChannel.prototype.playout = function(callback) {
  var THIS = this;
  var stopped = false;
  var timer = this.timer;
  var interval = 20;
  var timer_fun = function(cb) {
    console.log("playout "+ THIS.chan_id);
    THIS._play();
    if(!THIS.playing) {
      stopped = true;
      console.log("channel stopped!!");
      callback();
    }
  }
  var reschedule = function(info) {
    if(stopped) {
      console.log("record stopped");
      return;
    }
    var delta = Math.round(info.waitTime/1000000);
    var val; 
    if(delta > interval) {
      val = (interval<<1)-delta;
    } else {
      val = interval;
    }
    if(this.timer_hook) {
      val = this.timer_hook(val);
    }

    console.log("reschedule timer "+THIS.chan_id+" after "+val);
    timer.setTimeout(timer_fun, [], ''+val+'m', reschedule);
  };
  //timer.setTimeout(timer_fun, [], DR_PACKET_TIME_STR, reschedule);
  timer.setTimeout(timer_fun, [], '1s', reschedule);
};


function DRClient(args) {
  console.log("args:"+JSON.stringify(args));
  this.server_port = args.server_port;
  this.server_ip = args.server_ip || "localhost";
  this.socket = null;
  this.caller = args.caller;
  this.caller_port = -1;
  this.caller_file = null;
  this.called = args.called;
  this.called_port = -1;
  this.called_file = null;
  this.lost_rate = args.lost_rate; //integer, range is [0,100]
  this.jump_frac = args.jump_frac || 0;
  this.call_id = Math.ceil(Math.random()*65535);
  this.request_id = 0;
  this.state = DR_CLIENT_STATE_INIT;
  this.when_over = null;
  this.when_error = null;
  this.samples_per_seconds = 8000;
  this.play_interval_str = '10m';
  this.play_interval = 10;
  this.max_jump = this.jump_frac * this.play_interval;
  this.packet_size = this.samples_per_seconds / (1000/this.play_interval);
  var THIS = this;
  if(this.lost_rate) {
    this.send_hook = function(socket, buf, offset, size, port, ip, callback) {
      if((Math.random()*100) < THIS.lost_rate) {
        process.nextTick(function() {callback(ERROR_LOST);}); //skip send packet
      } else {
        socket.send(buf, offset, size, port, ip, callback);
      }
    }
  }
  if(this.jump_frac) {
    this.timer_hook = function(val) {
      var jump = Math.random()*THIS.max_jump;
      if(Math.random() < 0.5) {
        jump = -jump;
      }
      return val + jump;
    }
  }
}

//require('util').inherits(DRClient, events.EventEmitter);

DRClient.prototype._on_received = function(msg, rinfo) {
  console.log("receive message:"+this.state);
  if(this.state == DR_CLIENT_STATE_INVITE) { //invite response
    try {
      var ack = mtg_proto.parse_record_start_ack(msg);
      console.log("ack:"+JSON.stringify(ack));
      this.state = DR_CLIENT_STATE_PLAY;
      this._play_all_channes(ack);
    } catch(ex) {
      console.log("_on_received failed:"+ex+", "+ex.stack);
      this._emit_error(ex);
    }
  }
}

DRClient.prototype._emit_error = function(error) {
  if(this.when_error) {
    this.when_error(error);
  }
  if(this.socket) {
    this.socket.close();
  }
}

DRClient.prototype._emit_over = function() {
  if(this.when_over) {
    this.when_over(arguments);
  }
  if(this.socket) {
    this.socket.close();
  }
}

DRClient.prototype._channel_play = function(chan_id, peer_ip, peer_port, wavfile, callback) {
  var THIS = this;
  var file = fs.readFile(wavfile, function(error, data) {
    if(error) {
      console.log("read wavfile error:"+error);
      THIS._emit_error(error);
      return;
    } else {
      var channel = new DRChannel({chan_id:chan_id, peer_ip:peer_ip, peer_port:peer_port, data:data, send_hook:THIS.send_hook});
      channel.play_when_ready(function() {
        channel.playout(callback);
      });
      //channel.playout(callback);
      if(chan_id == "caller") {
        THIS.caller_channel = channel;
      } else {
        THIS.called_channel = channel;
      }
    }
  });
}

DRClient.prototype._play_all_channes = function(ack) {
  var THIS = this;
  var ms_ip = net_utils.long2addr(ack.ms_ip);
  console.log("ms_ip:"+ms_ip);
  async.parallel([function(callback) {
      THIS._channel_play("caller", ms_ip, ack.caller_port, THIS.caller_file, callback);
    }, function(callback) {
      THIS._channel_play("called", ms_ip, ack.called_port, THIS.called_file, callback);
    }
  ], function(error) {
    if(error) {
      THIS._emit_error(error);
    } else {
      THIS._emit_over();
    }
  });
}

DRClient.prototype.stop_record = function() {
  console.log("stop record:"+this.call_id);
  var message = mtg_proto.make_record_stop(this.request_id, this.call_id);
  socket.send(message, 0, message.length, this.server_port, this.server_ip, function(error, bytes) {
    if(error) {
      THIS._emit_error(error);
      return;
    } else {
      THIS.request_id++;
      THIS.state = DR_CLIENT_STATE_INVITE;
      return;
    }
  });
};

DRClient.prototype.start_record = function(index) {
  var THIS = this;
  var socket = dgram.createSocket('udp4');
  var message = mtg_proto.make_record_start(this.request_id, this.call_id, this.caller, this.called);
  socket.on("message", function(msg, rinfo) {
    THIS._on_received(msg, rinfo);
  });
  console.log("server port:"+this.server_port);
  console.log('sending record start');
  socket.send(message, 0, message.length, this.server_port, this.server_ip, function(error, bytes) {
    if(error) {
      console.log('sent record start failed!');
      THIS._emit_error(error);
      return;
    } else {
      console.log('sent record start '+index);
      THIS.request_id++;
      THIS.state = DR_CLIENT_STATE_INVITE;
      return;
    }
  });
  this.socket = socket;
  return this;
};

DRClient.prototype.caller_play = function(filepath) {
  this.caller_file = filepath;
  return this;
}

DRClient.prototype.called_play = function(filepath) {
  this.called_file = filepath;
  return this;
}

DRClient.prototype.when = function(event_name, handler) {
  if(event_name == "over") {
    this.when_over = handler;
  } else if(event_name == "error") {
    this.when_error = handler;
  }
  return this;
}

function nconf_get_str(key, default_val) {
  var val = nconf.get(key);
  if(val === undefined) {
    return default_val; 
  } else {
    return val.toString();
  }
}

function play_once(index, callback) {
  console.log("play once "+index);
  var client = new DRClient({"server_port":nconf.get("port"), "server_ip":nconf.get("ip")
    , "caller":nconf_get_str("caller"), "called":nconf_get_str("called"), "lost_rate":nconf.get("lost_rate"), 'jump_frac':nconf.get("jump_frac")});
  client.start_record(index).caller_play(nconf.get("caller-file")).called_play(nconf.get("called-file"))
    .when("over", function() {
      console.log(""+index+" play over!");
      callback();
    }).when("error", function(error) {
      console.log(""+index+" error:"+error);
      callback();
    });
}

function play_always(index, callback) {
  var fun = function() {
    play_once(index, fun);
  };
  play_once(index, fun);
}

function play(info /*object: index, always*/, callback) {
  try {
    if(info.always) {
      play_always(info.index, callback);
    } else {
      play_once(info.index, callback);
    }
  } catch(ex) {
    console.log(""+info.index+" dr_play error:"+ex+", "+ex.stack);
    //console.log("usage: node dr_play.js: --port=<port> --ip=<ip> --caller=<caller number> --called=<called number> --caller-file=<caller a-law file>")
  }
}

function play_all() {
  var clients_count = nconf.get("count");
  var always = nconf.get("always");
  var client_index = [];
  for(var i = 0; i < clients_count; ++i) {
    client_index.push({index:i, always:always});
  }
  async.map(client_index, play, function() {
    console.log("play all over!!");
    process.exit(0);
  });
}

play_all();

